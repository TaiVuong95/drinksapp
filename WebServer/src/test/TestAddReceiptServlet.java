import Beans.DrinksReceipt;
import Beans.Receipt;
import Servlet.AddReceiptServlet;
import org.json.JSONObject;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;

public class TestAddReceiptServlet {
    @Test
    void jsonToReceipt(){
        String string="{\"DATE\":\"2018-05-17\",\"TOTAL_COST\":18000,\"ADDRESS\":\"Quận 7\",\"PHONE_NUMBER\":\"0868981730\",\"USERNAME\":\"thienquang\",\"NAME\":\"Nguyễn Thiện Quang\",\"LISTDRINKS\":[{\"DRINKSNAME\":\"Cà Phê Sữa Đá\",\"QUANTITY\":1}]}";
        JSONObject jsonObject=new JSONObject(string);
        Receipt receipt=AddReceiptServlet.jsonToReceipt(jsonObject);

        Receipt result=new Receipt();
        result.setName("Nguyễn Thiện Quang");
        result.setDate("2018-05-17");
        result.setAddress("Quận 7");
        result.setPhoneNumber("0868981730");
        result.setTotalCost(18000);
        ArrayList<DrinksReceipt> list=new ArrayList<>();
        list.add(new DrinksReceipt("Cà Phê Sữa Đá",1,null));
        result.setDrinksReceiptArrayList(list);

        assertEquals(result,receipt);
    }
}
