package Beans;

public class DrinksReceipt {
    String DrinksName;
    Integer Quantity;
    Integer Price;

    public DrinksReceipt(String drinksName, Integer quantity, Integer price) {
        DrinksName = drinksName;
        Quantity = quantity;
        Price = price;
    }

    public String getDrinksName() {
        return DrinksName;
    }

    public void setDrinksName(String drinksName) {
        DrinksName = drinksName;
    }

    public Integer getQuantity() {
        return Quantity;
    }

    public void setQuantity(Integer quantity) {
        Quantity = quantity;
    }

    public Integer getPrice() {
        return Price;
    }

    public void setPrice(Integer price) {
        Price = price;
    }
}
