package Beans;

public class Drinks {
    private String drinksID;
    private String drinksName;
    private int price;
    private String description;
    private boolean state;
    private byte[] imageData;
    private String Type;

    public Drinks(String drinksID, String drinksName, int price, String description, boolean state, byte[] imageData, String type) {
        this.drinksID = drinksID;
        this.drinksName = drinksName;
        this.price = price;
        this.description = description;
        this.state = state;
        this.imageData = imageData;
        this.Type = type;
    }

    public String getDrinksID() {
        return drinksID;
    }

    public void setDrinksID(String drinksID) {
        this.drinksID = drinksID;
    }

    public String getDrinksName() {
        return drinksName;
    }

    public void setDrinksName(String drinksName) {
        this.drinksName = drinksName;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isState() {
        return state;
    }

    public void setState(boolean state) {
        this.state = state;
    }

    public byte[] getImageData() {
        return imageData;
    }

    public void setImageData(byte[] imageData) {
        this.imageData = imageData;
    }

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }
}
