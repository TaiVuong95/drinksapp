package Beans;

import java.util.ArrayList;

public class Receipt {
    Integer ReceiptID;
    String Date;
    Integer TotalCost;
    String Address;
    String PhoneNumber;
    String Username;
    String Name;
    ArrayList<DrinksReceipt> drinksReceiptArrayList;

    public Receipt(){};

    public Receipt(Integer receiptID, String date, Integer totalCost, String address, String phoneNumber, String username, String name, ArrayList<DrinksReceipt> drinksReceiptArrayList) {
        ReceiptID = receiptID;
        Date = date;
        TotalCost = totalCost;
        Address = address;
        PhoneNumber = phoneNumber;
        Username = username;
        Name = name;
        this.drinksReceiptArrayList = drinksReceiptArrayList;
    }

    public Integer getReceiptID() {
        return ReceiptID;
    }

    public void setReceiptID(Integer receiptID) {
        ReceiptID = receiptID;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }

    public Integer getTotalCost() {
        return TotalCost;
    }

    public void setTotalCost(Integer totalCost) {
        TotalCost = totalCost;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public String getPhoneNumber() {
        return PhoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        PhoneNumber = phoneNumber;
    }

    public String getUsername() {
        return Username;
    }

    public void setUsername(String username) {
        Username = username;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public ArrayList<DrinksReceipt> getDrinksReceiptArrayList() {
        return drinksReceiptArrayList;
    }

    public void setDrinksReceiptArrayList(ArrayList<DrinksReceipt> drinksReceiptArrayList) {
        this.drinksReceiptArrayList = drinksReceiptArrayList;
    }
}
