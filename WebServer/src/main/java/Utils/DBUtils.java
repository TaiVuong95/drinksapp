package Utils;

import Beans.Drinks;
import Beans.DrinksReceipt;
import Beans.Receipt;
import Beans.UserAccount;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DBUtils {
    public static UserAccount findUser(Connection conn, //
                                       String userName, String password) throws SQLException {

        String sql = "Select * from USER_ACCOUNT a where a.USERNAME = ? and a.PASSWORD= ?";

        PreparedStatement pstm = conn.prepareStatement(sql);
        pstm.setString(1, userName);
        pstm.setString(2, password);
        ResultSet rs = pstm.executeQuery();

        if (rs.next()) {
            String gender = rs.getString("GENDER");
            String phone = rs.getString("PHONE_NUMBER");
            String email = rs.getString("EMAIL");
            String name = rs.getString("NAME");
            String address = rs.getString("ADDRESS");
            return new UserAccount(userName, password, gender, phone, email, name, address);
        }
        return null;
    }

    public static UserAccount findUser(Connection conn, String userName) throws SQLException {

        String sql = "Select * from USER_ACCOUNT where USER_ACCOUNT.USERNAME = ? ";

        PreparedStatement pstm = conn.prepareStatement(sql);
        pstm.setString(1, userName);

        ResultSet rs = pstm.executeQuery();

        if (rs.next()) {
            String password = rs.getString("PASSWORD");
            String gender = rs.getString("GENDER");
            String phone = rs.getString("PHONE_NUMBER");
            String email = rs.getString("EMAIL");
            String name = rs.getString("NAME");
            String address = rs.getString("ADDRESS");
            return new UserAccount(userName, password, gender, phone, email, name, address);

        }
        return null;
    }

    //Quen mat khau
    public static void insertUser(Connection conn, UserAccount userAccount) throws SQLException {
        String sql = "Insert into USER_ACCOUNT(USERNAME, PASSWORD, GENDER, PHONE_NUMBER, EMAIL, NAME, ADDRESS) values (?,?,?,?,?,?,?)";

        PreparedStatement pstm = conn.prepareStatement(sql);

        pstm.setString(1, userAccount.getUserName());
        pstm.setString(2, userAccount.getPassword());
        pstm.setString(3, userAccount.getGender());
        pstm.setString(4, userAccount.getPhoneNumber());
        pstm.setString(5, userAccount.getEmail());
        pstm.setString(6, userAccount.getName());
        pstm.setString(7, userAccount.getAddress());

        pstm.executeUpdate();
    }

    public static void updateUser(Connection conn, UserAccount userAccount) throws SQLException {
        String sql = "Update USER_ACCOUNT set PASSWORD =?, GENDER=?, PHONE_NUMBER=?, EMAIL=?, NAME=?, ADDRESS=? where USERNAME=?";

        PreparedStatement pstm = conn.prepareStatement(sql);

        pstm.setString(1, userAccount.getPassword());
        pstm.setString(2, userAccount.getGender());
        pstm.setString(3, userAccount.getPhoneNumber());
        pstm.setString(4, userAccount.getEmail());
        pstm.setString(5, userAccount.getName());
        pstm.setString(6, userAccount.getAddress());
        pstm.setString(7, userAccount.getUserName());

        pstm.executeUpdate();
    }

    //Drinks
    public static List<Drinks> queryDrinks(Connection conn) throws SQLException {
        String sql = "Select * from DRINKS INNER JOIN CATEGORY ON DRINKS.CATEGORY_CATEGORYID=CATEGORY.CATEGORYID";

        PreparedStatement pstm = conn.prepareStatement(sql);

        ResultSet rs = pstm.executeQuery();
        List<Drinks> list = new ArrayList<Drinks>();
        while (rs.next()) {
            String drinksID = rs.getString("DRINKSID");
            String drinksName = rs.getString("DRINKSNAME");
            int price = rs.getInt("PRICE");
            String description = rs.getString("DESCRIPTION");
            boolean state = rs.getBoolean("STATE");
            byte[] imageData = rs.getBytes("IMAGE");
            String type = rs.getString("CATEGORYNAME");
            Drinks drinks = new Drinks(drinksID, drinksName, price, description, state, imageData, type);
            list.add(drinks);
        }
        return list;
    }

    public static List<Drinks> queryCategoryDrinks(Connection conn, String CATEGORYID) throws SQLException {
        String sql = "Select * from DRINKS INNER JOIN CATEGORY ON DRINKS.CATEGORY_CATEGORYID=CATEGORY.CATEGORYID where CATEGORY.CATEGORYID=?";

        PreparedStatement pstm = conn.prepareStatement(sql);
        pstm.setString(1, CATEGORYID);

        ResultSet rs = pstm.executeQuery();
        List<Drinks> list = new ArrayList<Drinks>();
        while (rs.next()) {
            String drinksID = rs.getString("DRINKSID");
            String drinksName = rs.getString("DRINKSNAME");
            int price = rs.getInt("PRICE");
            String description = rs.getString("DESCRIPTION");
            boolean state = rs.getBoolean("STATE");
            byte[] imageData = rs.getBytes("IMAGE");
            String type = rs.getString("CATEGORYNAME");
            Drinks drinks = new Drinks(drinksID, drinksName, price, description, state, imageData, type);
            list.add(drinks);
        }
        return list;
    }

    public static Drinks findDrinks(Connection conn, String drinksID) throws SQLException {
        String sql = "Select * from DRINKS INNER JOIN CATEGORY ON DRINKS.CATEGORY_CATEGORYID=CATEGORY.CATEGORYID where DRINKS.DRINKSID=?";
        PreparedStatement pstm = conn.prepareStatement(sql);
        pstm.setString(1, drinksID);

        ResultSet rs = pstm.executeQuery();

        while (rs.next()) {
            String drinksName = rs.getString("DRINKSNAME");
            int price = rs.getInt("PRICE");
            String description = rs.getString("DESCRIPTION");
            boolean state = rs.getBoolean("STATE");
            byte[] imageData = rs.getBytes("IMAGE");
            String type = rs.getString("CATEGORYNAME");
            Drinks drinks = new Drinks(drinksID, drinksName, price, description, state, imageData, type);
            return drinks;
        }
        return null;
    }

    public static void updateDrinks(Connection conn, Drinks drinks) throws SQLException {
        String sql = "Update DRINKS set DRINKSNAME =?, PRICE=?, DESCRIPTION=?, STATE=?, IMAGE=? , CATEGORY_CATEGORYID=(SELECT CATEGORYID FROM CATEGORY WHERE CATEGORY.CATEGORYNAME=? ) where DRINKSID=? ";

        PreparedStatement pstm = conn.prepareStatement(sql);

        pstm.setString(1, drinks.getDrinksName());
        pstm.setInt(2, drinks.getPrice());
        pstm.setString(3, drinks.getDescription());
        pstm.setBoolean(4, drinks.isState());
        pstm.setBytes(5, drinks.getImageData());
        pstm.setString(6, drinks.getType());
        pstm.setString(7, drinks.getDrinksID());
        pstm.executeUpdate();
    }

    public static void insertDrinks(Connection conn, Drinks drinks) throws SQLException {
        String sql = "Insert into DRINKS(DRINKSNAME, PRICE, DESCRIPTION, STATE, IMAGE, CATEGORY_CATEGORYID) values (?,?,?,?,?,(SELECT CATEGORYID FROM CATEGORY WHERE CATEGORY.CATEGORYNAME=? ))";

        PreparedStatement pstm = conn.prepareStatement(sql);

        pstm.setString(1, drinks.getDrinksName());
        pstm.setInt(2, drinks.getPrice());
        pstm.setString(3, drinks.getDescription());
        pstm.setBoolean(4, drinks.isState());
        pstm.setBytes(5, drinks.getImageData());
        pstm.setString(6, drinks.getType());
        pstm.executeUpdate();
    }

    public static void deleteDrinks(Connection conn, String drinksID) throws SQLException {
        String sql = "Delete From DRINKS where DRINKSID= ?";

        PreparedStatement pstm = conn.prepareStatement(sql);

        pstm.setInt(1, Integer.parseInt(drinksID));

        pstm.executeUpdate();
    }

    public static String getCateloryID(Connection conn, String type) throws SQLException {
        String sql = "Select CATEGORYID from CATEGORY where CATEGORYNAME=?";

        PreparedStatement pstm = conn.prepareStatement(sql);
        pstm.setString(1, type);

        ResultSet rs = pstm.executeQuery();


        while (rs.next()) {
            String string = rs.getString("CATEGORYID");
            return string;
        }
        return null;
    }

    public static ArrayList<Receipt> getReceipt(Connection conn, String username) throws SQLException {
        String sql = "Select * from RECEIPTS INNER JOIN RECEIPTS_has_DRINKS ON RECEIPTS.RECEIPTSID=RECEIPTS_has_DRINKS.RECEIPTS_RECEIPTSID INNER JOIN DRINKS ON RECEIPTS_has_DRINKS.DRINKS_DRINKSID=DRINKS.DRINKSID where USER_ACCOUNT_USERNAME=?";
        PreparedStatement pstm = conn.prepareStatement(sql);
        pstm.setString(1, username);

        ResultSet rs = pstm.executeQuery();

        ArrayList<Receipt> arrayList = new ArrayList<>();

        while (rs.next()) {
            String ReceiptID = rs.getString("RECEIPTSID");
            String Date = rs.getString("DATE");
            String TotalCost = rs.getString("TOTAL_COST");
            String Address = rs.getString("ADDRESS");
            String PhoneNumber = rs.getString("PHONE_NUMBER");
            String Username = rs.getString("USER_ACCOUNT_USERNAME");
            String DrinksName = rs.getString("DRINKSNAME");
            String Quantity = rs.getString("QUANTITY");
            Integer Price = rs.getInt("PRICE");
            String Name = rs.getString("NAME");
            DrinksReceipt drinksReceipt = new DrinksReceipt(DrinksName, Integer.parseInt(Quantity), Price);
            if (arrayList.isEmpty()) {
                ArrayList<DrinksReceipt> drinksReceiptArrayList = new ArrayList<>();
                drinksReceiptArrayList.add(drinksReceipt);
                Receipt receipt = new Receipt(Integer.parseInt(ReceiptID), Date, Integer.parseInt(TotalCost), Address, PhoneNumber, Username, Name, drinksReceiptArrayList);
                arrayList.add(receipt);
            } else {
                boolean check = false;
                for (int i = 0; i < arrayList.size(); i++) {
                    if (arrayList.get(i).getReceiptID() == Integer.parseInt(ReceiptID)) {
                        arrayList.get(i).getDrinksReceiptArrayList().add(drinksReceipt);
                        check = true;
                        break;
                    }
                }
                if (check == false) {
                    ArrayList<DrinksReceipt> drinksReceiptArrayList = new ArrayList<>();
                    drinksReceiptArrayList.add(drinksReceipt);
                    Receipt receipt = new Receipt(Integer.parseInt(ReceiptID), Date, Integer.parseInt(TotalCost), Address, PhoneNumber, Username, Name, drinksReceiptArrayList);
                    arrayList.add(receipt);
                }
            }

        }
        return arrayList;
    }


    public static void insertReceipt(Connection conn, Receipt receipt) throws SQLException {
        //Insert Receipt
        String sql = "Insert into RECEIPTS(DATE,TOTAL_COST,ADDRESS,PHONE_NUMBER,NAME,USER_ACCOUNT_USERNAME) values (?,?,?,?,?,?)";
        PreparedStatement pstm = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
        pstm.setString(1, receipt.getDate());
        pstm.setInt(2, receipt.getTotalCost());
        pstm.setString(3, receipt.getAddress());
        pstm.setString(4, receipt.getPhoneNumber());
        pstm.setString(5, receipt.getName());
        pstm.setString(6, receipt.getUsername());
        pstm.executeUpdate();

        //Get ReceiptID

        ResultSet rs = pstm.getGeneratedKeys();

        String ReceiptID = "";
        if (rs.next()) {
            ReceiptID = rs.getString(1);
        }

        if (ReceiptID != "") {
            sql = "INSERT into RECEIPTS_has_DRINKS(RECEIPTS_RECEIPTSID,DRINKS_DRINKSID,QUANTITY) values (?,(SELECT DRINKSID from DRINKS where DRINKS.DRINKSNAME=?),?)";
            pstm = conn.prepareStatement(sql);
            ArrayList<DrinksReceipt> drinksReceipts = receipt.getDrinksReceiptArrayList();
            for (int i = 0; i < drinksReceipts.size(); i++) {
                pstm.setString(1, ReceiptID);
                pstm.setString(2, drinksReceipts.get(i).getDrinksName());
                pstm.setInt(3, drinksReceipts.get(i).getQuantity());

                pstm.executeUpdate();
            }
        }
    }

    //InsertRate
    public static String updateRate(Connection conn, Integer Rate, Integer DrinksID, String Username) throws SQLException {
        String sql = "select MAX(RECEIPTSID) from RECEIPTS_has_DRINKS inner join RECEIPTS on RECEIPTS_has_DRINKS.RECEIPTS_RECEIPTSID=RECEIPTS.RECEIPTSID" +
                " where (USER_ACCOUNT_USERNAME=? and DRINKS_DRINKSID=?)";

        PreparedStatement pstm = conn.prepareStatement(sql);
        pstm.setString(1, Username);
        pstm.setInt(2, DrinksID);

        ResultSet rs=pstm.executeQuery();

        String ReceiptID = "";
        if (rs.next()) {
            ReceiptID = rs.getString(1);
        }

        if(ReceiptID!=null){
            Integer NumRate=-1;
            sql="SELECT NUMRATE from RECEIPTS_has_DRINKS where (RECEIPTS_RECEIPTSID=? and DRINKS_DRINKSID=?)";

            pstm=conn.prepareStatement(sql);
            pstm.setString(1,ReceiptID);
            pstm.setInt(2,DrinksID);

            rs=pstm.executeQuery();
            if (rs.next()) {
                NumRate=rs.getInt(1);
            }

            if (NumRate==-1){
                sql="update RECEIPTS_has_DRINKS set NUMRATE=? where (RECEIPTS_RECEIPTSID=? and DRINKS_DRINKSID=?)";

                pstm=conn.prepareStatement(sql);

                pstm.setInt(1,Rate);
                pstm.setString(2,ReceiptID);
                pstm.setInt(3,DrinksID);

                pstm.executeUpdate();

                return "Cảm ơn bạn đã đánh giá!";
            }else{
                return "Bạn đã đánh giá sản phẩm!";
            }

        }
        else {
            return "Bạn chưa dùng sản phẩm này! Bạn có thể mua để trải nghiệm!";
        }

    }

    public static Integer getRateDrinks(Connection conn,String DrinksID) throws SQLException {
        Integer rate=-1;

        String sql="select sum(NUMRATE),count(*) from RECEIPTS_has_DRINKS where (DRINKS_DRINKSID=? and NUMRATE!=-1) group by DRINKS_DRINKSID";

        PreparedStatement pstm=conn.prepareStatement(sql);

        pstm.setString(1,DrinksID);

        ResultSet rs= pstm.executeQuery();
        Integer sum=0;
        Integer count=0;
        if (rs.next()){
            sum=rs.getInt(1);
            count=rs.getInt(2);
        }
        if(sum!=0 && count!=0){
            return sum/count;
        }

        return rate;
    }

    public static void changePassword(Connection conn,String Username,String Password) throws SQLException {
        String sql="UPDATE USER_ACCOUNT set PASSWORD=? where USERNAME=?";
        PreparedStatement pstm=conn.prepareStatement(sql);
        pstm.setString(1,Password);
        pstm.setString(2,Username);

        pstm.executeUpdate();

    }

    public static String findUserEmail(Connection conn,String mail) throws SQLException {
        String sql="select USERNAME from USER_ACCOUNT where EMAIL=?";
        String username="";
        PreparedStatement pstm=conn.prepareStatement(sql);
        pstm.setString(1,mail);

        ResultSet rs=pstm.executeQuery();
        if (rs.next()){
            username=rs.getString("USERNAME");
        }

        return username;

    }
}
