package Servlet;

import Utils.DBUtils;
import Utils.MyUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

@WebServlet(urlPatterns = {"/ChangePass"})
public class ChangePasswordServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    public ChangePasswordServlet() {
        super();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request,response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String username=request.getParameter("username");
        String password=request.getParameter("password");
        String errorString="";

        Connection conn=MyUtils.getStoredConnection(request);

        try {
            DBUtils.changePassword(conn,username,password);
            errorString="Bạn đã đổi mật khẩu thành công!";
        } catch (SQLException e) {
            e.printStackTrace();
            errorString=e.getMessage();
        }

        request.setAttribute("errorString", errorString);
        request.setAttribute("changepassword", password);

        // Send lỗi cho App

        response.setContentType("text/html");
        response.setCharacterEncoding("UTF-8");

        response.getWriter().write(errorString);
    }
}
