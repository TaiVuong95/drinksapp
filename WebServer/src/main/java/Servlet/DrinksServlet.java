package Servlet;

import Beans.Drinks;
import Utils.DBUtils;
import Utils.MyUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import javax.persistence.criteria.CriteriaBuilder;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Base64;import java.util.List;



@WebServlet(urlPatterns = {"/DrinksServlet"})
public class DrinksServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    public DrinksServlet() {
        super();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<Drinks> list = null;
        Integer index=Integer.parseInt(request.getParameter("index"));

        Connection conn = MyUtils.getStoredConnection(request);
        try {
            list = DBUtils.queryDrinks(conn);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        JSONArray jsonArray = new JSONArray();
        if (index+6> list.size()){
            for (int i = index; i < list.size(); i++) {
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("DrinksID", list.get(i).getDrinksID());
                    jsonObject.put("DrinksName", list.get(i).getDrinksName());
                    jsonObject.put("Price", list.get(i).getPrice());
                    jsonObject.put("Description", list.get(i).getDescription());
                    jsonObject.put("State", list.get(i).isState());
                    jsonObject.put("Image", Base64.getEncoder().encodeToString(list.get(i).getImageData()));
                    jsonObject.put("Type", list.get(i).getType());
                    try {
                        Integer rate=DBUtils.getRateDrinks(conn,list.get(i).getDrinksID());
                        if (rate!=-1) {
                            jsonObject.put("Rate", rate);
                        }else{
                            jsonObject.put("Rate", 0);
                        }
                    } catch (SQLException e) {
                        e.printStackTrace();
                        jsonObject.put("Rate", 0);
                    }
                    jsonArray.put(jsonObject);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }else {

            for (int i = index; i < index+6; i++) {
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("DrinksID", list.get(i).getDrinksID());
                    jsonObject.put("DrinksName", list.get(i).getDrinksName());
                    jsonObject.put("Price", list.get(i).getPrice());
                    jsonObject.put("Description", list.get(i).getDescription());
                    jsonObject.put("State", list.get(i).isState());
                    jsonObject.put("Image", Base64.getEncoder().encodeToString(list.get(i).getImageData()));
                    jsonObject.put("Type", list.get(i).getType());
                    try {
                        Integer rate=DBUtils.getRateDrinks(conn,list.get(i).getDrinksID());
                        if (rate!=-1) {
                            jsonObject.put("Rate", rate);
                        }else{
                            jsonObject.put("Rate", 0);
                        }
                    } catch (SQLException e) {
                        e.printStackTrace();
                        jsonObject.put("Rate", 0);
                    }
                    jsonArray.put(jsonObject);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");

        response.getWriter().write(String.valueOf(jsonArray));

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

}
