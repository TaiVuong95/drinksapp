package Servlet;

import Beans.UserAccount;
import Utils.DBUtils;
import Utils.MyUtils;
import org.json.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

@WebServlet("/UserInfoServlet")
public class UserInfoServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    public UserInfoServlet() {
        super();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request,response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String userName = request.getParameter("userName");

        UserAccount user = null;
        boolean hasError = false;
        String errorString = null;

        response.setStatus(HttpServletResponse.SC_OK);

        Connection conn = MyUtils.getStoredConnection(request);
        try {
            // Tìm user trong DB.
            user = DBUtils.findUser(conn, userName);

            if (user == null) {
                hasError = true;
                errorString = "Tài khoản không tồn tại, vui lòng thử lại!";
            }
        } catch (SQLException e) {
            e.printStackTrace();
            hasError = true;
            errorString = e.getMessage();
        }
        if (hasError == true) {

            response.setContentType("text/html");
            response.setCharacterEncoding("UTF-8");
            response.getWriter().write(errorString);
        } else {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("Name", user.getName());
            jsonObject.put("Gender", user.getGender());
            jsonObject.put("Phone", user.getPhoneNumber());
            jsonObject.put("Email", user.getEmail());
            jsonObject.put("Address", user.getAddress());

            response.setContentType("application/json");

            response.setCharacterEncoding("UTF-8");
            response.getWriter().write(String.valueOf(jsonObject));
        }

    }

}
