package Servlet;

import Beans.Receipt;
import Utils.DBUtils;
import Utils.MyUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

@WebServlet(urlPatterns = {"/HistoryShopping"})
public class HistoryServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;

    public HistoryServlet() {
        super();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request,response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String username=request.getParameter("Username");

        Connection conn = MyUtils.getStoredConnection(request);

        ArrayList<Receipt> receiptArrayList=null;
        String errorString=null;

        if (username==null){
            errorString="Không nhận dạng được tài khoản";

            response.setContentType("text/html");
            response.setCharacterEncoding("UTF-8");
            response.getWriter().write(errorString);
        }
        else{
            try {
                receiptArrayList=DBUtils.getReceipt(conn,username);
            } catch (SQLException e) {
                e.printStackTrace();
            }
            if (receiptArrayList.isEmpty()){
                errorString="Bạn chưa mua sản phảm nào từ cửa hàng";

                response.setContentType("text/html");
                response.setCharacterEncoding("UTF-8");
                response.getWriter().write(errorString);
            }
            else{
                JSONArray jsonArray=new JSONArray();
                for (int i=0;i<receiptArrayList.size();i++){
                    JSONObject jsonObject=new JSONObject();
                    jsonObject.put("RECEIPTSID",receiptArrayList.get(i).getReceiptID());
                    jsonObject.put("DATE",receiptArrayList.get(i).getDate());
                    jsonObject.put("TOTAL_COST",receiptArrayList.get(i).getTotalCost());
                    jsonObject.put("ADDRESS",receiptArrayList.get(i).getAddress());
                    jsonObject.put("PHONE_NUMBER",receiptArrayList.get(i).getPhoneNumber());
                    JSONArray jsonArray1=new JSONArray();
                    for (int j=0;j<receiptArrayList.get(i).getDrinksReceiptArrayList().size();j++){
                        JSONObject jsonObject1=new JSONObject();
                        jsonObject1.put("DRINKSNAME",receiptArrayList.get(i).getDrinksReceiptArrayList().get(j).getDrinksName());
                        jsonObject1.put("QUANTITY",receiptArrayList.get(i).getDrinksReceiptArrayList().get(j).getQuantity());
                        jsonObject1.put("PRICE",receiptArrayList.get(i).getDrinksReceiptArrayList().get(j).getPrice());
                        jsonArray1.put(jsonObject1);
                    }
                    jsonObject.put("LISTDRINKS",jsonArray1);
                    jsonArray.put(jsonObject);
                }

                response.setContentType("application/json");
                response.setCharacterEncoding("UTF-8");
                response.getWriter().write(String.valueOf(jsonArray));
            }
        }

    }
}
