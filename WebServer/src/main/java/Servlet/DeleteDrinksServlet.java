package Servlet;

import Beans.Drinks;
import Utils.DBUtils;
import Utils.MyUtils;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Base64;

@WebServlet(urlPatterns = {"/deleteDrinks"})
public class DeleteDrinksServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    public DeleteDrinksServlet() {
        super();
    }

    // Hiển thị trang sửa sản phẩm.
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Connection conn = MyUtils.getStoredConnection(request);

        String drinksID = (String) request.getParameter("DrinksID");

        String errorString = null;

        try {
            DBUtils.deleteDrinks(conn, drinksID);
            errorString="Delete Complete";
        } catch (SQLException e) {
            e.printStackTrace();
            errorString = e.getMessage();
        }



        // Lưu thông tin vào request attribute trước khi forward sang views.
        request.setAttribute("errorString", errorString);

        response.sendRedirect(request.getContextPath() + "/drinksList");

    }
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }
}
