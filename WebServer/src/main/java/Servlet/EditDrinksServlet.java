package Servlet;

import Beans.Drinks;
import Utils.DBUtils;
import Utils.MyUtils;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Base64;

@WebServlet(urlPatterns = {"/editDrinks"})
public class EditDrinksServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    public EditDrinksServlet() {
        super();
    }

    // Hiển thị trang sửa sản phẩm.
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Connection conn = MyUtils.getStoredConnection(request);

        String drinksID = (String) request.getParameter("DrinksID");

        Drinks drinks = null;

        String errorString = null;

        try {
            drinks = DBUtils.findDrinks(conn, drinksID);
        } catch (SQLException e) {
            e.printStackTrace();
            errorString = e.getMessage();
        }

        // Không có lỗi.
        // Sản phẩm không tồn tại để edit.
        // Redirect sang trang danh sách sản phẩm.
        if (errorString != null && drinks == null) {
            response.sendRedirect(request.getServletPath() + "/drinksList");
            return;
        }
        String string = Base64.getEncoder().encodeToString(drinks.getImageData());

        // Lưu thông tin vào request attribute trước khi forward sang views.
        request.setAttribute("errorString", errorString);
        request.setAttribute("ImageString", string);
        request.setAttribute("drinks", drinks);

        RequestDispatcher dispatcher = request.getServletContext()
                .getRequestDispatcher("/WEB-INF/views/editDrinksView.jsp");
        dispatcher.forward(request, response);

    }

    // Sau khi người dùng sửa đổi thông tin sản phẩm, và nhấn Submit.
    // Phương thức này sẽ được thực thi.
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Connection conn = MyUtils.getStoredConnection(request);

        String drinksID = (String) request.getParameter("DrinksID");
        String drinksName = (String) request.getParameter("DrinksName");
        String priceStr = (String) request.getParameter("Price");
        String description = (String) request.getParameter("Description");
        String stateStr = (String) request.getParameter("State");
        String imageStr = (String) request.getParameter("Image");
        String type = (String) request.getParameter("Type");
        Integer price = 0;
        boolean state = false;
        byte[] imageData = new byte[0];
        try {
            price = Integer.parseInt(priceStr);
            state = Boolean.parseBoolean(stateStr);
            imageData = Base64.getDecoder().decode(imageStr);
        } catch (Exception e) {
        }
        Drinks drinks = new Drinks(drinksID, drinksName, price, description, state, imageData, type);

        String errorString = null;

        try {
            DBUtils.updateDrinks(conn, drinks);
        } catch (SQLException e) {
            e.printStackTrace();
            errorString = e.getMessage();
        }
        // Lưu thông tin vào request attribute trước khi forward sang views.
        request.setAttribute("errorString", errorString);
        request.setAttribute("drinks", drinks);

        // Nếu có lỗi forward sang trang edit.
        if (errorString != null) {
            RequestDispatcher dispatcher = request.getServletContext()
                    .getRequestDispatcher("/WEB-INF/views/editDrinksView.jsp");
            dispatcher.forward(request, response);
        }
        // Nếu mọi thứ tốt đẹp.
        // Redirect sang trang danh sách sản phẩm.
        else {
            response.sendRedirect(request.getContextPath() + "/drinksList");
        }
    }

}
