package Servlet;

import Beans.UserAccount;
import Utils.DBUtils;
import Utils.MyUtils;
import org.json.HTTP;
import org.json.JSONException;
import org.json.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.sql.Connection;

@WebServlet(urlPatterns = {"/SignUpServlet"})
public class SignUpServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    public SignUpServlet() {
        super();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.getOutputStream().println("SignUpServlet");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String SignUp=request.getParameter("SignUp");
        String errorString = null;
        Connection connection = MyUtils.getStoredConnection(request);

        try {
            JSONObject jsonObject = new JSONObject(SignUp);
            UserAccount userAccount = jsonToUser(jsonObject);
            if (DBUtils.findUser(connection, userAccount.getUserName()) == null) {
                DBUtils.insertUser(connection, userAccount);
                errorString = "Đăng ký thành công";
            } else {
                errorString = "Tài khoản đã tồn tại,vui lòng thử tài khoản khác";
            }
            request.setAttribute("errorString", errorString);
            request.setAttribute("user", userAccount);
        } catch (Exception e) {
            e.printStackTrace();
            errorString = e.getMessage();
        }
        response.setContentType("text/html");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(errorString);
    }

    public static UserAccount jsonToUser(JSONObject jsonObject) {
        try {
            String UserName = jsonObject.getString("UserName");
            String Password = jsonObject.getString("Password");
            String Gender = jsonObject.getString("Gender");
            String Phone = jsonObject.getString("Phone");
            String Email = jsonObject.getString("Email");
            String Name = jsonObject.getString("Name");
            String Address = jsonObject.getString("Address");
            return new UserAccount(UserName, Password, Gender, Phone, Email, Name, Address);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }
}
