package Servlet;

import Utils.DBUtils;
import Utils.MyUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;


@WebServlet(urlPatterns = {"/Rating"})
public class RatingServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    public RatingServlet() {
        super();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request,response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Float rate= Float.valueOf(request.getParameter("Rate"));
        String DrinksID= request.getParameter("DrinksID");
        String Username=request.getParameter("Username");
        String errorString="";

        Connection conn=MyUtils.getStoredConnection(request);

        try {
            errorString=DBUtils.updateRate(conn,Math.round(rate),Integer.parseInt(DrinksID),Username);
        } catch (SQLException e) {
            e.printStackTrace();
            errorString=e.getMessage();
        }

        // Lưu các thông tin vào request attribute trước khi forward.
        request.setAttribute("errorString", errorString);
        request.setAttribute("rate", rate);

        // Send lỗi cho App

        response.setContentType("text/html");
        response.setCharacterEncoding("UTF-8");

        response.getWriter().write(errorString);
    }
}
