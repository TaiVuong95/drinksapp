package Servlet;

import Beans.DrinksReceipt;
import Beans.Receipt;
import Beans.UserAccount;
import Utils.DBUtils;
import Utils.MyUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

@WebServlet(urlPatterns = {"/addReceipt"})
public class AddReceiptServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    public AddReceiptServlet() {
        super();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request,response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //Receive Data
        String ReceiptData=request.getParameter("Receipt");

        String errorString=null;
        Connection connection=MyUtils.getStoredConnection(request);

        if (ReceiptData!=null){
            //Convert Data
            JSONObject jsonObject=new JSONObject(ReceiptData);
            Receipt receipt=jsonToReceipt(jsonObject);

            try {
                UserAccount userAccount=DBUtils.findUser(connection,receipt.getUsername());
                if (userAccount!=null){
                    DBUtils.insertReceipt(connection,receipt);
                    errorString="Thanh toán thành công";
                    request.setAttribute("receipt", receipt);
                }
                else{
                    errorString="Tài khoản của bạn không tồn tại!";
                }

            } catch (SQLException e) {
                e.printStackTrace();
                errorString=e.getMessage();
            }
        }else{
            errorString="Lỗi nhận dữ liệu!";
        }

        request.setAttribute("errorString", errorString);
        response.setContentType("text/html");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(errorString);

    }

    public static Receipt jsonToReceipt(JSONObject jsonObject) {
        try {
            String Date=jsonObject.getString("DATE");
            Integer Total_Cost=jsonObject.getInt("TOTAL_COST");
            String Address=jsonObject.getString("ADDRESS");
            String PhoneNumber=jsonObject.getString("PHONE_NUMBER");
            String Username=jsonObject.getString("USERNAME");
            String Name=jsonObject.getString("NAME");
            JSONArray jsonArray=new JSONArray();
            jsonArray=jsonObject.getJSONArray("LISTDRINKS");
            ArrayList<DrinksReceipt> arrayList=new ArrayList<>();
            for (int i=0;i<jsonArray.length();i++){
                JSONObject jsonObject1=new JSONObject();
                jsonObject1=jsonArray.getJSONObject(i);
                DrinksReceipt drinksReceipt=new DrinksReceipt(jsonObject1.getString("DRINKSNAME"),jsonObject1.getInt("QUANTITY"),0);
                arrayList.add(drinksReceipt);
            }
            Receipt receipt=new Receipt(0,Date,Total_Cost,Address,PhoneNumber,Username,Name,arrayList);
            return receipt;
        } catch (JSONException e) {
            e.printStackTrace();
        }


        return null;
    }
}
