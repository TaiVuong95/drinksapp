package Servlet;

import Utils.DBUtils;
import Utils.MyUtils;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

@WebServlet(urlPatterns = {"/ForgotPassword"})
public class ForgotPassServlet  extends HttpServlet {
    private static final long serialVersionUID = 1L;

    public ForgotPassServlet() {
        super();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request,response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String to=request.getParameter("ForgotPassword");

        String from="cnpmgroup26@gmail.com";
        String fromPass="group_26";
        String errorString="";
        String password="";
        String host="smtp.gmail.com";
        String username="";
        Connection conn = MyUtils.getStoredConnection(request);
        try {
            username=DBUtils.findUserEmail(conn,to);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        if (username.equals("")){
            to="";
        }
        if (to!=""){



            Properties properties= System.getProperties();
            properties.put("mail.smtp.starttls.enable", "true");
            properties.put("mail.smtp.host", host);
            properties.put("mail.smtp.user", from);
            properties.put("mail.smtp.password", fromPass);
            properties.put("mail.smtp.port", "587");
            properties.put("mail.smtp.auth", "true");

            Session session = Session.getDefaultInstance(properties);

            try {
                MimeMessage message=new MimeMessage(session);

                message.setFrom(new InternetAddress(from));
                message.addRecipients(Message.RecipientType.TO, new InternetAddress[]{new InternetAddress(to)});

                message.setSubject("Password hiện tại của bạn!");

                password=generatePassword();
                message.setText(password);

                Transport transport=session.getTransport("smtp");
                transport.connect(host,from,fromPass);
                transport.sendMessage(message,message.getAllRecipients());
                transport.close();
                errorString="Bạn vào mail xem password của bạn!";

                try {
                    DBUtils.changePassword(conn,username,password);
                } catch (SQLException e) {
                    e.printStackTrace();
                }

            } catch (MessagingException e) {
                e.printStackTrace();
            }
        }
        else {
            errorString="Email không đúng! Vui lòng nhập lại!";
        }

        // Lưu các thông tin vào request attribute trước khi forward.
        request.setAttribute("errorString", errorString);
        request.setAttribute("forgotPassword", password);

        // Send lỗi cho App

        response.setContentType("text/html");
        response.setCharacterEncoding("UTF-8");

        response.getWriter().write(errorString);

    }


    String generatePassword(){
        String password="";
        String Upcase = "QWERTYUIOPASDFGHJKLZXCVBNM";
        String Lowcase=Upcase.toLowerCase();
        String number="1234567890";

        String string=Upcase+Lowcase+number;

        for (int i = 0; i < 10; i++) {
            int temp = (int) Math.round(Math.random() * string.length());
            password += string.charAt(temp);
        }

        return password;
    }
}
