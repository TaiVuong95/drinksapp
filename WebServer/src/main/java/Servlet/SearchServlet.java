package Servlet;

import Beans.Drinks;
import Connect.ConnectionUtils;
import Utils.DBUtils;
import Utils.MyUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.regex.Pattern;
@WebServlet(urlPatterns = {"/SearchServlet"})
public class SearchServlet extends HttpServlet {
    public SearchServlet() {
        super();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String DrinksName = request.getParameter("DrinksName");
        String Search=request.getParameter("Search");
        Integer index=Integer.parseInt(request.getParameter("index"));

        String errorString=null;
        List<Drinks> list = null;

        Connection connection = MyUtils.getStoredConnection(request);

        try {
            list = DBUtils.queryDrinks(connection);
        } catch (SQLException e) {
            e.printStackTrace();
            errorString=e.getMessage();
        }

        if (Search!="" && DrinksName==null){
            int i=0;

            while (list!=null){
                if (i==list.size()){
                    break;
                }
                else{
                    if (removeAccent(list.get(i).getDrinksName().toUpperCase()).indexOf(removeAccent(Search.toUpperCase())) == -1) {
                        list.remove(i);
                        i--;
                    }
                }
                i++;
            }

            request.setAttribute("errorString", errorString);
            request.setAttribute("drinksList", list);

            // Forward sang /WEB-INF/views/drinksListView.jsp
            RequestDispatcher dispatcher = request.getServletContext()
                    .getRequestDispatcher("/WEB-INF/views/drinksListView.jsp");
            dispatcher.forward(request, response);
        }
        else if (DrinksName!=null){
            JSONArray jsonArray = new JSONArray();

            if (list != null) {
                ArrayList<Drinks> results=new ArrayList<>();
                for (int i = 0; i < list.size(); i++) {
                    if (list.get(i).getDrinksName().toUpperCase().indexOf(DrinksName.toUpperCase()) != -1) {
                        results.add(list.get(i));
                    }
                }

                if (index+8> results.size()){
                    for (int i = index; i < results.size(); i++) {
                        JSONObject jsonObject = new JSONObject();
                        try {
                            jsonObject.put("DrinksID", results.get(i).getDrinksID());
                            jsonObject.put("DrinksName", results.get(i).getDrinksName());
                            jsonObject.put("Price", results.get(i).getPrice());
                            jsonObject.put("Description", results.get(i).getDescription());
                            jsonObject.put("State", results.get(i).isState());
                            jsonObject.put("Image", Base64.getEncoder().encodeToString(results.get(i).getImageData()));
                            jsonObject.put("Type", results.get(i).getType());
                            try {
                                Integer rate=DBUtils.getRateDrinks(connection,results.get(i).getDrinksID());
                                if (rate!=-1) {
                                    jsonObject.put("Rate", rate);
                                }else{
                                    jsonObject.put("Rate", 0);
                                }
                            } catch (SQLException e) {
                                e.printStackTrace();
                                jsonObject.put("Rate", 0);
                            }
                            jsonArray.put(jsonObject);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }else {

                    for (int i = index; i < index+8; i++) {
                        JSONObject jsonObject = new JSONObject();
                        try {
                            jsonObject.put("DrinksID", results.get(i).getDrinksID());
                            jsonObject.put("DrinksName", results.get(i).getDrinksName());
                            jsonObject.put("Price", results.get(i).getPrice());
                            jsonObject.put("Description", results.get(i).getDescription());
                            jsonObject.put("State", results.get(i).isState());
                            jsonObject.put("Image", Base64.getEncoder().encodeToString(results.get(i).getImageData()));
                            jsonObject.put("Type", results.get(i).getType());
                            jsonArray.put(jsonObject);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");

            response.getWriter().write(String.valueOf(jsonArray));
        }
        else {
            // Forward sang /WEB-INF/views/drinksListView.jsp
            RequestDispatcher dispatcher = request.getServletContext()
                    .getRequestDispatcher("/WEB-INF/views/drinksListView.jsp");
            dispatcher.forward(request, response);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

    public String covertStringToURL(String str) {
        try {
            String temp = Normalizer.normalize(str, Normalizer.Form.NFD);
            Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
            return pattern.matcher(temp).replaceAll("").toLowerCase().replaceAll(" ", "-").replaceAll("đ", "d");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String removeAccent(String s) {

        String temp = Normalizer.normalize(s, Normalizer.Form.NFD);
        Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
        return pattern.matcher(temp).replaceAll("").replace('đ', 'd').replace('Đ', 'D');
    }
}
