package Servlet;

import Beans.Drinks;
import Utils.DBUtils;
import Utils.MyUtils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Base64;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

@WebServlet(urlPatterns = { "/addDrinks" })
@MultipartConfig(fileSizeThreshold = 1024 * 1024 * 2, // 2MB
        maxFileSize = 1024 * 1024 * 10, // 10MB
        maxRequestSize = 1024 * 1024 * 50) // 50MB
public class AddDrinksServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    public AddDrinksServlet() {
        super();
    }

    // Hiển thị trang tạo sản phẩm.
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        RequestDispatcher dispatcher = request.getServletContext()
                .getRequestDispatcher("/WEB-INF/views/addDrinksView.jsp");
        dispatcher.forward(request, response);
    }

    // Khi người dùng nhập các thông tin sản phẩm, và nhấn Submit.
    // Phương thức này sẽ được gọi.
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Connection conn = MyUtils.getStoredConnection(request);

        String drinksName = (String) request.getParameter("DrinksName");
        String priceStr = (String) request.getParameter("Price");
        String description = (String) request.getParameter("Description");
        String stateStr = (String) request.getParameter("State");
        String type = (String) request.getParameter("Type");
        Part part=request.getPart("Image");

        Integer price = 0;
        boolean state = false;

        try {
            price = Integer.parseInt(priceStr);
            state = Boolean.parseBoolean(stateStr);
        } catch (Exception e) {
        }
        Drinks drinks = new Drinks("", drinksName, price, description, state, part.getInputStream().readAllBytes(), type);

        String errorString = null;

        // Mã sản phẩm phải là chuỗi chữ [a-zA-Z_0-9]
        // Có ít nhất một ký tự.
        String regex = "\\w+";

        if (drinksName == null || !drinksName.matches(regex)) {
            errorString = "Drinks Name invalid!";
        }

        if (errorString == null) {
            try {
                DBUtils.insertDrinks(conn, drinks);
            } catch (SQLException e) {
                e.printStackTrace();
                errorString = e.getMessage();
            }
        }

        // Lưu thông tin vào request attribute trước khi forward sang views.
        request.setAttribute("errorString", errorString);
        request.setAttribute("drinks", drinks);

        // Nếu có lỗi forward (chuyển tiếp) sang trang 'edit'.
        if (errorString != null) {
            RequestDispatcher dispatcher = request.getServletContext()
                    .getRequestDispatcher("/WEB-INF/views/addDrinksView.jsp");
            dispatcher.forward(request, response);
        }
        // Nếu mọi thứ tốt đẹp.
        // Redirect (chuyển hướng) sang trang danh sách sản phẩm.
        else {
            response.sendRedirect(request.getContextPath() + "/listDrinks");
        }
    }

}