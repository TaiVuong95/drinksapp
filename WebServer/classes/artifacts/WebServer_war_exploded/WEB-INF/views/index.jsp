<%--
  Created by IntelliJ IDEA.
  User: thienquang
  Date: 4/19/18
  Time: 12:55 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Quản trị hệ thống</title>
    <c:set var="root" value="${pageContext.request.contextPath}"/>
    <link href="${root}/admin/mos-css/mos-style.css" rel='stylesheet' type='text/css'/>
    <script src="${root}/js/jquery-1.11.1.min.js"></script>
</head>
<body>
<jsp:include page="header.jsp"></jsp:include>
<div id="wrapper">
    <jsp:include page="menu.jsp"></jsp:include>

    <div id="rightContent">
        <div class="quoteOfDay">
            <b>Chào bạn! :</b><br>
            <i style="color: #5b5b5b;">"If you think you can, you really can"</i>
        </div>
    </div>
    <div class="clear"></div>

    <jsp:include page="footer.jsp"></jsp:include>
</div>


</body>
</html>
