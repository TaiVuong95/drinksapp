<%@ page import="Beans.Drinks" %><%--
  Created by IntelliJ IDEA.
  User: thienquang
  Date: 4/19/18
  Time: 4:49 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Sửa thông tin đồ uống</title>
    <c:set var="root" value="${pageContext.request.contextPath}"/>
    <link href="${root}/admin/mos-css/mos-style.css" rel='stylesheet' type='text/css' />
    <script src="${root}/js/jquery-1.11.1.min.js"></script>
<body>

<jsp:include page="header.jsp"></jsp:include>
<div id="wrapper">
    <jsp:include page="menu.jsp"></jsp:include>

    <div id="rightContent">
        <h3>Thêm đồ uống</h3>
        <form method="POST" action="${pageContext.request.contextPath}/addDrinks" enctype="multipart/form-data" onsubmit="return confirm('Bấm vào nút OK để tiếp tục');">
            <table width="95%">
                <tr>
                    <td width="125px"><b>Select Image:</b></td>
                    <td><input type="file" name="Image" class="button"></td>
                </tr>
                <tr>
                    <td><b>Drinks Name</b></td>
                    <td><input type="text" name="DrinksName" class="sedang"></td>
                </tr>
                <tr>
                    <td><b>Price</b></td>
                    <td><input type="text" name="Price" class="pendek"></td>
                </tr>
                <tr>
                    <td><b>Type</b></td>
                    <td><input type="text" name="Type" class="sedang"></td>
                </tr>
                <tr>
                    <td><b>State</b></td>
                    <td>
                        <input type="radio" name="State" value="true">Còn hàng
                        <input type="radio" name="State" value="false">Hết hàng
                    </td>
                </tr>
                <tr>
                    <td><b>Description</b></td>
                    <td><textarea name="Description"></textarea></td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <input type="submit" class="button" value="Submit">
                        <input type="reset" class="button" value="Reset">
                    </td>
                </tr>
            </table>
        </form>
    </div>
    <div class="clear"></div>

    <jsp:include page="footer.jsp"></jsp:include>
</div>

</body>
</html>
