<%@ page import="Beans.Drinks" %><%--
  Created by IntelliJ IDEA.
  User: thienquang
  Date: 4/19/18
  Time: 4:49 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Sửa thông tin đồ uống</title>
    <c:set var="root" value="${pageContext.request.contextPath}"/>
    <link href="${root}/admin/mos-css/mos-style.css" rel='stylesheet' type='text/css' />
    <script src="${root}/js/jquery-1.11.1.min.js"></script>
<body>

<jsp:include page="header.jsp"></jsp:include>
<div id="wrapper">
    <jsp:include page="menu.jsp"></jsp:include>

    <div id="rightContent">
        <h3>Form</h3>
        <c:if test="${not empty drinks}">
            <form method="POST" action="${pageContext.request.contextPath}/editDrinks" onsubmit="return confirm('Bấm vào nút OK để tiếp tục');">
                <input type="hidden" name="DrinksID" value="${drinks.drinksID}"/>
                <table width="95%">
                    <tr>
                        <td width="125px"><b>Drinks Name</b></td>
                        <td><input type="text" name="DrinksName" value="${drinks.drinksName}" class="sedang"></td>
                    </tr>
                    <input type="hidden" name="Image" value="${ImageString}"/>
                    <tr>
                        <td><img src="data:image/jpeg;base64,<%= request.getAttribute("ImageString") %>" width="125"
                                 height="125" border="1"></td>
                    </tr>
                    <tr>
                        <td><b>Price</b></td>
                        <td><input type="text" name="Price" value="${drinks.price}" class="pendek"></td>
                    </tr>
                    <tr>
                        <td><b>Type</b></td>
                        <td><input type="text" name="Type" value="${drinks.type}" class="sedang"></td>
                    </tr>
                    <tr>
                        <td><b>State</b></td>
                        <td>
                            <% Drinks drinks = (Drinks) request.getAttribute("drinks");
                                if (drinks.isState()) { %>
                            <input type="radio" name="State" value="true" checked="checked">Còn hàng
                            <input type="radio" name="State" value="false">Hết hàng
                            <% } else { %>
                            <input type="radio" name="State" value="true">Còn hàng
                            <input type="radio" name="State" value="false" checked="checked">Hết hàng
                            <% } %>
                        </td>
                    </tr>
                    <tr>
                        <td><b>Description</b></td>
                        <td><textarea name="Description">${drinks.description}</textarea></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <input type="submit" class="button" value="Submit">
                            <input type="reset" class="button" value="Reset">
                        </td>
                    </tr>
                </table>
            </form>
        </c:if>
    </div>
    <div class="clear"></div>

    <jsp:include page="footer.jsp"></jsp:include>
</div>

</body>
</html>
