<%--
  Created by IntelliJ IDEA.
  User: thienquang
  Date: 4/19/18
  Time: 2:53 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Drinks List</title>
    <c:set var="root" value="${pageContext.request.contextPath}"/>
    <link href="${root}/admin/mos-css/mos-style.css" rel='stylesheet' type='text/css' />
    <script src="${root}/js/jquery-1.11.1.min.js"></script>
</head>
<body>

<jsp:include page="header.jsp"></jsp:include>
<div id="wrapper">
    <jsp:include page="menu.jsp"></jsp:include>

    <div id="rightContent">
        <h3>Danh sách đồ uống </h3>
        <form method="GET" action="${pageContext.request.contextPath}/SearchServlet">
            <th class="data"> Search by name:</th></h3Search> <td><input type="text" name="Search" value="" class="sedang"> </td> <input type ="Submit" class="button" value="Search">
        </form>
        <table class="data">
            <tr class="data">
                <th class="data" width="30px">DrinksID</th>
                <th class="data">DrinksName</th>
                <th class="data">Price</th>
                <th class="data">State</th>
                <th class="data">Type</th>
                <th class="data" width="75px">Action</th>
            </tr>
            <c:forEach items="${drinksList}" var="drinks">
                <tr class="data">
                    <td class="data" width="30px">${drinks.drinksID}</td>
                    <td class="data">${drinks.drinksName}</td>
                    <td class="data">${drinks.price}</td>
                    <td class="data">${drinks.state}</td>
                    <td class="data">${drinks.type}</td>
                    <td class="data" width="75px">
                        <center>
                            <a href="editDrinks?DrinksID=${drinks.drinksID}"><img src="/admin/mos-css/img/edit.png"></a>&nbsp;&nbsp;&nbsp;
                            <a  href="#" onclick="RemoveFunc(${drinks.drinksID})"><img src="/admin/mos-css/img/remove.png"></a>
                            <script>
                                function RemoveFunc(value){
                                    if (confirm("Bấm vào nút OK để tiếp tục")){
                                        window.location = "deleteDrinks?DrinksID="+value;
                                    }
                                }
                            </script>
                        </center>
                    </td>
                </tr>
            </c:forEach>
        </table>
    </div>
    <div class="clear"></div>

    <jsp:include page="footer.jsp"></jsp:include>
</div>

</body>
</html>
