package com.example.group26.app.Model;

public class Receipt {
    private Integer receiptID;
    private String date;
    private Integer totalCost;
    private String address;
    private Integer phoneNumber;

    public Receipt(Integer receiptID, String date, Integer totalCost, String address, Integer phoneNumber) {
        this.receiptID = receiptID;
        this.date = date;
        this.totalCost = totalCost;
        this.address = address;
        this.phoneNumber = phoneNumber;
    }

    public Integer getReceiptID() {
        return receiptID;
    }

    public void setReceiptID(Integer receiptID) {
        this.receiptID = receiptID;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Integer getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(Integer totalCost) {
        this.totalCost = totalCost;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(Integer phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}
