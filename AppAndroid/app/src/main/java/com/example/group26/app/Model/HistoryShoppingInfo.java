package com.example.group26.app.Model;

public class HistoryShoppingInfo {
    String name;
    String historyInfo;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHistoryInfo() {
        return historyInfo;
    }

    public void setHistoryInfo(String historyInfo) {
        this.historyInfo = historyInfo;
    }

    public HistoryShoppingInfo(String name, String historyInfo) {
        this.name = name;
        this.historyInfo = historyInfo;
    }
}
