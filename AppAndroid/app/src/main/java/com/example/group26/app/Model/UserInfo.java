package com.example.group26.app.Model;
import com.example.group26.app.Adapter.UserInfoAdapter;
public class UserInfo {
    public Integer img;
    public String info;

    public UserInfo(Integer img, String info) {
        this.img = img;
        this.info = info;
    }
}
