package com.example.group26.app.Activity;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.example.group26.app.Fragment.CategoryFragment;
import com.example.group26.app.Fragment.FragmentUserInfo;
import com.example.group26.app.Fragment.HomeFragment;
import com.example.group26.app.Fragment.LoginSignUpFragment;
import com.example.group26.app.R;
import com.example.group26.app.Util.CheckConection;

public class MainActivity extends AppCompatActivity {
    public static String username;
    public static Boolean checkLogin=false;;
    public static Boolean checkUpdateInfo = false;
    public static Boolean checkLogout = false;

    private BottomNavigationView.OnNavigationItemSelectedListener onNavigationItemSelectedListener=new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            FragmentManager fragmentManager=getSupportFragmentManager();
            FragmentTransaction fragmentTransaction=fragmentManager.beginTransaction();
            switch (item.getItemId()){
                case R.id.actionHome: {
                    if(fragmentManager.findFragmentByTag("HomeFragment")==null){
                        fragmentTransaction.replace(R.id.fragmentContainer,new HomeFragment(),"HomeFragment");
                        fragmentTransaction.addToBackStack("HomeFragment");
                        fragmentTransaction.commit();
                    }else{
                            fragmentManager.popBackStack("HomeFragment",0);
                    }
                    return true;
                }
                case R.id.actionProfile: {

                    try{
                        if(checkLogin==false){
                            Intent intent=getIntent();
                            checkLogin=intent.getBooleanExtra("login",false);
                            if(checkLogin==true){
                                username=intent.getStringExtra("username");
                                //     Toast.makeText(MainActivity.this,username , Toast.LENGTH_SHORT).show();
                            }
                        }


                    }
                    catch (Exception ex)
                    {
                        Toast.makeText(MainActivity.this, "lỗi truyền dữ liệu", Toast.LENGTH_SHORT).show();
                        ex.printStackTrace();
                    }
                    try {
                        Intent intent=getIntent();
                        checkLogout=intent.getBooleanExtra("logout",false);
                        if(checkLogout==true){
                            checkLogin=false;
                        }
                    }
                    catch (Exception ex) {
                        ex.printStackTrace();
                    }
                    if(checkLogin==false) {
                        if (fragmentManager.findFragmentByTag("LoginSignUpFragment") == null) {
                            fragmentTransaction.replace(R.id.fragmentContainer, new LoginSignUpFragment(), "LoginSignUpFragment");
                            fragmentTransaction.addToBackStack("LoginSignUpFragment");
                            fragmentTransaction.commit();
                        } else {
                            fragmentManager.popBackStack("LoginSignUpFragment", 0);
                        }
                        return true;
                    }else{
                        if (fragmentManager.findFragmentByTag("FragmentUserInfo") == null) {
                            //send username to fragmentUserInfo
                            Bundle bundle = new Bundle();
                            bundle.putString("username", username );
                            FragmentUserInfo fragmentUserInfo=new FragmentUserInfo();
                            fragmentUserInfo.setArguments(bundle);
                            fragmentTransaction.replace(R.id.fragmentContainer, fragmentUserInfo, "FragmentUserInfo");
                            fragmentTransaction.addToBackStack("FragmentUserInfo");
                            fragmentTransaction.commit();
                        } else {
                            fragmentManager.popBackStack("FragmentUserInfo", 0);
                        }
                        return  true;
                    }
                }
                case R.id.actionCategory: {
                    if(fragmentManager.findFragmentByTag("CategoryFragment")==null){
                        fragmentTransaction.replace(R.id.fragmentContainer,new CategoryFragment(),"CategoryFragment");
                        fragmentTransaction.addToBackStack("CategoryFragment");
                        fragmentTransaction.commit();
                    }else{
                        fragmentManager.popBackStack("CategoryFragment",0);
                    }

                    return true;
                }
                case R.id.actionSearch: {
                    Intent intent = new Intent(MainActivity.this,SearchActivity.class);
                    startActivity(intent);

                    return true;
                }
                case R.id.actionShopping: {
                    Intent intent = new Intent(MainActivity.this,ShoppingCartActivity.class);
                    startActivity(intent);
                    return true;
                }
            }
            return false;
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            Drawable background = this.getResources().getDrawable(R.drawable.gradient_background);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(this.getResources().getColor(android.R.color.transparent));
            //window.setNavigationBarColor(this.getResources().getColor(android.R.color.transparent));
            window.setBackgroundDrawable(background);
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        initPermission();

        if (CheckConection.haveNetworkConnection(getApplicationContext())){
            intView();
        }
        else{
            CheckConection.ShowToast_Short(getApplicationContext(),"Bạn hãy kiểm tra lại kết nối");
            finish();
        }

    }

    public void intView(){

        /*--Menu bottom--*/
        BottomNavigationView bottomNavigationView=(BottomNavigationView) findViewById(R.id.Navbot);
        BottomNavigationViewHelper.disableShiftMode(bottomNavigationView);
        bottomNavigationView.setOnNavigationItemSelectedListener(onNavigationItemSelectedListener);

        FragmentManager fragmentManager=getSupportFragmentManager();
        FragmentTransaction fragmentTransaction=fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fragmentContainer,new HomeFragment(),"HomeFragment");
        fragmentTransaction.addToBackStack("HomeFragment");
        fragmentTransaction.commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar_menu,menu);
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == 1) {
            if (grantResults.length == 1 &&
                    grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(MainActivity.this, "Permision Write File is Granted", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(MainActivity.this, "Permision Write File is Denied", Toast.LENGTH_SHORT).show();

            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }
    private void initPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

                //Permisson don't granted .
                if (shouldShowRequestPermissionRationale(
                        Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    Toast.makeText(MainActivity.this, "Permission isn't granted ", Toast.LENGTH_SHORT).show();
                }
                // Permisson don't granted and dont show dialog again.
                else {
                    Toast.makeText(MainActivity.this, "Permisson don't granted and dont show dialog again ", Toast.LENGTH_SHORT).show();
                }
                //Register permission
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1);

            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        Intent intent=getIntent();
        //checkLogin=intent.getBooleanExtra("login",false);
        //username=intent.getStringExtra("username");
    }
}
