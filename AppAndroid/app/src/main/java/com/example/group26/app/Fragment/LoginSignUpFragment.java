package com.example.group26.app.Fragment;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.group26.app.Activity.ChangePassActivity;
import com.example.group26.app.Activity.ForgotPassword;
import com.example.group26.app.Activity.MainActivity;
import com.example.group26.app.Activity.PaymentLoginSignUpActivity;
import com.example.group26.app.Activity.PaymentReceiptActivity;
import com.example.group26.app.Model.URLServlet;
import com.example.group26.app.Model.User;
import com.example.group26.app.R;
import com.example.group26.app.Unittest.LoginPresenter;
import com.example.group26.app.Unittest.LoginView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static android.content.Context.MODE_PRIVATE;


public class LoginSignUpFragment extends Fragment implements LoginView {
    LoginPresenter presenter;
    User user;
    String tenThongTinDangNhap="Login";
    boolean checkLogin=false;
    boolean checkSinup=true;
    Button btnLogin;
    CheckBox chkSave;
    EditText edtUser,edtPass;
    TextView txtForgotPass;
    URLServlet urlServlet=new URLServlet();
    EditText edtName,edtUsername,edtPassword,edtEmail,edtPhone,edtAdress;
    Button btnSignup;


    RadioButton radMale,radFemale;String gender;
    RadioGroup radGroup;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_login_sign_up, container, false);

        //Event
        TabHost tabHost=view.findViewById(R.id.tabHost);
        tabHost.setup();

        TabHost.TabSpec tab1=tabHost.newTabSpec("t1");
        tab1.setIndicator("Đăng nhập");
        tab1.setContent(R.id.tab1);
        tabHost.addTab(tab1);

        TabHost.TabSpec tab2=tabHost.newTabSpec("t2");
        tab2.setIndicator("Đăng ký");
        tab2.setContent(R.id.tab2);
        tabHost.addTab(tab2);
        //Controls login
        edtUser = (EditText) view.findViewById(R.id.edtUser);
        edtPass = (EditText) view.findViewById(R.id.edtPass);
        btnLogin = (Button) view.findViewById(R.id.btnLogin);

        //Controls Signup
        edtName=view.findViewById(R.id.edtName);
        edtUsername=view.findViewById(R.id.edtUsername);
        edtPassword=view.findViewById(R.id.edtPassword);
        edtEmail=view.findViewById(R.id.edtEmail);
        edtPhone=view.findViewById(R.id.edtPhone);
        edtAdress=view.findViewById(R.id.edtAddress);

        btnSignup=view.findViewById(R.id.btnSignup);
        radMale=view.findViewById(R.id.radMale);
        radFemale=view.findViewById(R.id.radFemale);
        radGroup=view.findViewById(R.id.radGroup);

        chkSave=view.findViewById(R.id.chkSave);
        txtForgotPass=view.findViewById(R.id.txtForgotPassword);


        txtForgotPass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getActivity(), ForgotPassword.class);
                startActivity(intent);
            }
        });
        btnLogin.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if(edtPass.getText().toString().matches("")||edtUser.getText().toString().matches("")) {
                    Toast.makeText(getActivity(), "bạn điền thiếu thông tin,vui lòng kiểm tra lại", Toast.LENGTH_SHORT).show();
                    checkLogin=false;
                }
                else {
                    checkLogin=true;
                }
                if(checkLogin==true) {
                    sendDataLogin();

                }
            }
        });
        MainActivity mainActivity=new MainActivity();
        Bundle bundle=new Bundle();
        bundle.putBoolean("login_successfully",checkLogin);

        //Test
        btnSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = edtEmail.getText().toString().trim();
                String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

                if(edtName.getText().toString().matches("")) {
                    checkSinup = false;
                    Toast.makeText(getActivity(), "Họ tên không được rỗng", Toast.LENGTH_SHORT).show();
                } else if(edtUsername.getText().toString().length()<8 ){
                    checkSinup=false;
                    Toast.makeText(getActivity(), "Tên đăng nhập ít nhất 8 ký tự", Toast.LENGTH_SHORT).show();
                }else if(edtPassword.getText().toString().length()<8){
                    checkSinup=false;
                    Toast.makeText(getActivity(), "Mật khẩu ít nhất 8 ký tự", Toast.LENGTH_SHORT).show();
                }else if(!email.matches(emailPattern)) {
                    checkSinup=false;
                    Toast.makeText(getActivity(),"Email không hợp lệ", Toast.LENGTH_SHORT).show();
                } else if(edtPhone.getText().toString().length()<10 || edtPhone.getText().toString().length()>11 ){
                    checkSinup=false;
                    Toast.makeText(getActivity(), "Số điện thoại không hợp lệ", Toast.LENGTH_SHORT).show();
                }    else if(edtAdress.getText().toString().matches("")) {
                    checkSinup=false;
                    Toast.makeText(getActivity(), "Địa chỉ không được rỗng", Toast.LENGTH_SHORT).show();
                }
                {
                    switch (radGroup.getCheckedRadioButtonId()){
                        case R.id.radFemale:
                            gender="F";
                            break;
                        case R.id.radMale:
                            gender="M";
                            break;
                    }
                    if(radMale.isChecked()==false && radFemale.isChecked()==false)
                    {
                        checkSinup=false;
                        Toast.makeText(getActivity(), "Vui lòng chọn giới tính", Toast.LENGTH_SHORT).show();
                    }
                }

                if(checkSinup==true){
                    sendDataSignup();
                }

                checkSinup=true;
            }
        });

        // Inflate the layout for this fragment
        return  view;
    }
    public void sendDataLogin() {

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        StringRequest stringRequest = new StringRequest(Request.Method.POST, urlServlet.getURLLogoinServlet(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
               // Toast.makeText(getActivity(), response, Toast.LENGTH_SHORT).show();
                if(response.equals("Đăng nhập thành công")) {
                    Toast.makeText(getActivity(), "Đăng nhập thành công", Toast.LENGTH_SHORT).show();
                    if(PaymentLoginSignUpActivity.check){
                        MainActivity.username = edtUser.getText().toString();
                        MainActivity.checkLogin = true;
                        Intent intent = new Intent(getActivity(), PaymentReceiptActivity.class);
                        startActivity(intent);
                    }else{
                        Intent intent = new Intent(getActivity(), MainActivity.class);
                        //intent.putExtra("login",true);
                        //intent.putExtra("username",edtUser.getText().toString());
                        MainActivity.username = edtUser.getText().toString();
                        MainActivity.checkLogin = true;
                        startActivity(intent);
                    }

                }else {
                    Toast.makeText(getActivity(), "Tên đăng nhập hoặc mật khẩu không chính xác", Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getActivity(), "Lỗi!", Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> data = new HashMap<>();
                data.put("userName", edtUser.getText().toString().trim());
                data.put("password",edtPass.getText().toString().trim());
                return data;
            }
        };
        requestQueue.add(stringRequest);
    }
    public void sendDataSignup() {
        JSONObject jsonObject=new JSONObject();
        try {
            jsonObject.put("UserName",edtUsername.getText().toString());
            jsonObject.put("Password",edtPassword.getText().toString());
            jsonObject.put("Gender",gender);
            jsonObject.put("Phone",edtPhone.getText().toString());
            jsonObject.put("Email",edtEmail.getText().toString());
            jsonObject.put("Name",edtName.getText().toString());
            jsonObject.put("Address",edtAdress.getText().toString());

            RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
            StringRequest stringRequest = new StringRequest(Request.Method.POST, urlServlet.getURLSignUpServlet(), new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Toast.makeText(getActivity(), response, Toast.LENGTH_SHORT).show();
                    if(response.equals("Đăng ký thành công")){

                        edtUser.setText(edtUsername.getText().toString());
                        edtPass.setText(edtPassword.getText().toString());

                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(getActivity(), "Lỗi!", Toast.LENGTH_SHORT).show();
                }
            })
            {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String,String> data = new HashMap<>();
                    data.put("SignUp", jsonObject.toString());
                    return data;
                }
            };
            requestQueue.add(stringRequest);
        }

        catch (JSONException e)
        {
            e.printStackTrace();
        }


    }
    public String Gender(String gender) {

        switch (radGroup.getCheckedRadioButtonId()){
            case R.id.radFemale:
                gender="F";
                break;
            case R.id.radMale:
                gender="M";
        }
        return gender;
    }

    @Override
    public void onPause() {
        super.onPause();
        SharedPreferences preferences=getActivity().getSharedPreferences(tenThongTinDangNhap,MODE_PRIVATE);
        SharedPreferences.Editor editor=preferences.edit();
        editor.putString("UserName",edtUser.getText().toString());
        editor.putString("PassWord",edtPass.getText().toString());
        editor.putBoolean("SAVE",chkSave.isChecked());
        editor.commit();
    }

    @Override
    public void onResume() {
        super.onResume();
        SharedPreferences preferences=getActivity().getSharedPreferences(tenThongTinDangNhap,MODE_PRIVATE);
        String userName=preferences.getString("UserName","");
        String passWord=preferences.getString("PassWord","");
        boolean save=preferences.getBoolean("SAVE",false);
        if(save){
            edtUser.setText(userName);
            edtPass.setText(passWord);
        }
    }


    @Override
    public String getUsername() {
        return edtUser.getText().toString();
    }

    @Override
    public String getPassword() {
        return edtPass.getText().toString();
    }
}
