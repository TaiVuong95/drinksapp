package com.example.group26.app.Util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.Toast;

public class CheckConection {
    public static boolean haveNetworkConnection(Context context) {

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo=cm.getActiveNetworkInfo();
        if (netInfo!=null){
            return true;
        }
        else return false;
    }
    public static void ShowToast_Short(Context context,String notification){
        Toast.makeText(context, notification,Toast.LENGTH_SHORT).show();
    }
}
