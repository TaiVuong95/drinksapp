package com.example.group26.app.Adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.group26.app.Model.Drinks;
import com.example.group26.app.R;
import com.example.group26.app.Activity.DetailProductActivity;


import java.text.DecimalFormat;
import java.util.ArrayList;


public class SearchAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    public interface ILoadMore{
        void onLoadMore();
    }

    private final int VIEW_TYPE_DRINKS=0,VIEW_TYPE_LOADING=1;
    Context context;
    ArrayList<Drinks> listDrinks;
    ILoadMore loadMore;
    boolean isLoading;
    int visibleItemCount;
    int totalItemCount,pastVisiblesItems;
    RecyclerView recyclerView;
    private int lastPosition=-1;

    public SearchAdapter(RecyclerView recyclerView, Context context, ArrayList<Drinks> listDrinks) {
        this.context = context;
        this.listDrinks = listDrinks;
        this.recyclerView=recyclerView;

        final GridLayoutManager gridLayoutManager=(GridLayoutManager) recyclerView.getLayoutManager();
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy>0){
                    visibleItemCount=gridLayoutManager.getChildCount();
                    totalItemCount=gridLayoutManager.getItemCount();
                    pastVisiblesItems=gridLayoutManager.findFirstVisibleItemPosition();
                    if (!isLoading&&(visibleItemCount + pastVisiblesItems) >= totalItemCount){
                        if (loadMore!=null){
                            loadMore.onLoadMore();
                        }
                        isLoading=true;
                    }
                }
            }
        });
    }

    @Override
    public int getItemViewType(int position) {

        return listDrinks.get(position) == null ? VIEW_TYPE_LOADING:VIEW_TYPE_DRINKS;
    }
    public void setLoadMore(ILoadMore loadMore) {
        this.loadMore = loadMore;
    }
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType==VIEW_TYPE_DRINKS) {
            View v= LayoutInflater.from(parent.getContext()).inflate(R.layout.drinks,parent,false);

            Animation animation = AnimationUtils.loadAnimation(recyclerView.getContext(), R.anim.push_left_in);
            animation.setDuration(500);
            v.startAnimation(animation);

            return new ItemHolder(v);
        }else if (viewType==VIEW_TYPE_LOADING){
            View v= LayoutInflater.from(parent.getContext()).inflate(R.layout.item_loading,parent,false);
            return new LoadingViewHolder(v);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        if (viewHolder instanceof ItemHolder){
            Drinks drinks=listDrinks.get(position);
            ItemHolder holder= (ItemHolder) viewHolder;
            holder.tvDrinksName.setText(drinks.getDrinksName());
            DecimalFormat decimalFormat=new DecimalFormat("###,###,###");
            holder.tvPrice.setText("Giá : "+decimalFormat.format(drinks.getPrice())+"Đ");
            holder.ivDrinks.setImageBitmap(BitmapFactory.decodeByteArray(drinks.getImageData(),0,drinks.getImageData().length));
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context,DetailProductActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("Drinks", drinks);
                    intent.putExtra("Du lieu",bundle);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                }
            });
            setAnimation(holder.itemView,position);
        }
        else if (viewHolder instanceof LoadingViewHolder){
            LoadingViewHolder loadingViewHolder=(LoadingViewHolder) viewHolder;
            loadingViewHolder.progressBar.setIndeterminate(true);
        }
    }

    @Override
    public int getItemCount() {
        return listDrinks.size();
    }

    public void setLoaded() {
        isLoading = false;
    }

    public class ItemHolder extends RecyclerView.ViewHolder{
        public ImageView ivDrinks;
        public TextView tvDrinksName,tvPrice;

        public ItemHolder(View itemView) {
            super(itemView);
            ivDrinks=(ImageView) itemView.findViewById(R.id.ivDrinkss);
            tvDrinksName=(TextView) itemView.findViewById(R.id.tvDrinksNames);
            tvPrice=(TextView) itemView.findViewById(R.id.tvPrices);
        }
    }

    public class LoadingViewHolder extends RecyclerView.ViewHolder{

        public ProgressBar progressBar;

        public LoadingViewHolder(View itemView) {
            super(itemView);
            progressBar = itemView.findViewById(R.id.progressBar);
        }
    }

    private void setAnimation(View viewToAnimate, int position)
    {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition)
        {
            Animation animation = AnimationUtils.loadAnimation(recyclerView.getContext(), R.anim.push_left_in);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }
}