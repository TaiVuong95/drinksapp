package com.example.group26.app.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.group26.app.Model.UserInfo;
import com.example.group26.app.R;

import java.util.List;

public class UserInfoAdapter extends BaseAdapter {
    Context myContext;
    int myLayout;
    List<UserInfo>arrayUserInfo;
    public  UserInfoAdapter(Context context,int layout,List<UserInfo>userInfoList){
        myContext=context;
        myLayout=layout;
        arrayUserInfo=userInfoList;
    }
    @Override
    public int getCount() {
        return arrayUserInfo.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater= (LayoutInflater) myContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView=inflater.inflate(myLayout,null);
        ImageView img=convertView.findViewById(R.id.img);
        img.setImageResource(arrayUserInfo.get(position).img);
        TextView txtInfo=convertView.findViewById(R.id.txtInfo);
        txtInfo.setText(arrayUserInfo.get(position).info);
        return convertView;
    }
}
