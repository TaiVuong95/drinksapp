package com.example.group26.app.Activity;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.group26.app.Adapter.ItemsPaymentAdapter;
import com.example.group26.app.Model.URLServlet;
import com.example.group26.app.Model.User;
import com.example.group26.app.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import cn.refactor.lib.colordialog.PromptDialog;


public class PaymentReceiptActivity extends AppCompatActivity {
    TextView textViewTenKhachHang, textViewDiaChiGiaoHang, textViewSoDienThoai, textViewTongGiaTien;
    Button buttonChinhSua, buttonThanhToan;
    String username;
    URLServlet urlServlet = new URLServlet();
    User user;
    RequestQueue requestQueue;
    SQLiteDatabase database;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            Drawable background = this.getResources().getDrawable(R.drawable.gradient_background);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(this.getResources().getColor(android.R.color.transparent));
            window.setBackgroundDrawable(background);

        }
        setContentView(R.layout.payment_receipt);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarTimKiem);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        textViewTenKhachHang = findViewById(R.id.tvTenKhachHang);
        textViewDiaChiGiaoHang = findViewById(R.id.tvDiaChiGiaoHang);
        textViewSoDienThoai = findViewById(R.id.tvSoDienThoai);
        textViewTongGiaTien = findViewById(R.id.tvTongGiaTien);

        textViewTongGiaTien.setText(ShoppingCartActivity.textViewTongGiaTien.getText().toString());

        username = MainActivity.username;

        if (!MainActivity.checkUpdateInfo) {
            getUserInfo();
        } else {
            textViewTenKhachHang.setText(EditInfoPaymentActivity.HoVaTen);
            textViewDiaChiGiaoHang.setText(EditInfoPaymentActivity.DiaChi+", "+EditInfoPaymentActivity.DiaChi2);
            textViewSoDienThoai.setText(EditInfoPaymentActivity.SoDienThoai);
        }

        buttonChinhSua = findViewById(R.id.btnChinhSua);
        buttonChinhSua.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!MainActivity.checkUpdateInfo) {
                    Intent intent = new Intent(PaymentReceiptActivity.this, EditInfoPaymentActivity.class);
                    intent.putExtra("HoVaTen", user.getName());
                    //intent.putExtra("DiaChi", user.getAddress());
                    intent.putExtra("SoDienThoai", user.getPhone());
                    startActivity(intent);
                } else {
                    Intent intent = new Intent(PaymentReceiptActivity.this, EditInfoPaymentActivity.class);
                    intent.putExtra("HoVaTen", textViewTenKhachHang.getText().toString());
                    intent.putExtra("DiaChi", textViewDiaChiGiaoHang.getText().toString());
                    intent.putExtra("SoDienThoai", textViewSoDienThoai.getText().toString());
                    startActivity(intent);
                }

            }
        });

        buttonThanhToan = findViewById(R.id.btnThanhToan);
        buttonThanhToan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDataForReceipt();

                DataShoppingCart dataShoppingCart = new DataShoppingCart(PaymentReceiptActivity.this);
                database = dataShoppingCart.getWritableDatabase();
                database.execSQL("DELETE FROM "+DataShoppingCart.TB_SHOPPINGCART);

                new PromptDialog(PaymentReceiptActivity.this)
                        .setDialogType(PromptDialog.DIALOG_TYPE_SUCCESS)
                        .setAnimationEnable(true)
                        .setTitleText("Thành công!")
                        .setContentText("Cám ơn bạn đã mua dùng sản phẩm.")
                        .setPositiveListener("QUAY VỀ TRANG CHỦ", new PromptDialog.OnPositiveListener() {
                            @Override
                            public void onClick(PromptDialog dialog) {
                                dialog.dismiss();
                                Intent intent = new Intent(PaymentReceiptActivity.this, MainActivity.class);
                                startActivity(intent);
                            }
                        }).show();
            }
        });

        ItemsPaymentAdapter itemsPaymentAdapter = new ItemsPaymentAdapter(this, ShoppingCartActivity.listDrinks);
        NonScrollListView listView = findViewById(R.id.lvItemsPayment);
        listView.setAdapter(itemsPaymentAdapter);
        listView.setFocusable(false);

    }

    //Hàm lấy dữ liệu cho Receipt
    private void getDataForReceipt() {
        // Ngày tháng
        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String date = sdf.format(c.getTime());

        //Total Cost
        int totalCost = ShoppingCartActivity.totalCost;

        // Tên khách hàng
        String name = textViewTenKhachHang.getText().toString();

        // ADDRESS
        String address = textViewDiaChiGiaoHang.getText().toString();

        //Phone number
        String phoneNumber = textViewSoDienThoai.getText().toString();

        // Username
        String username2 = MainActivity.username;

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("DATE", date);
            jsonObject.put("TOTAL_COST", totalCost);
            jsonObject.put("ADDRESS", address);
            jsonObject.put("PHONE_NUMBER", phoneNumber);
            jsonObject.put("USERNAME", username2);
            jsonObject.put("NAME", name);
            JSONArray jsonArray = new JSONArray();
            for (int i = 0; i < ShoppingCartActivity.listDrinksInReceipt.size(); i++) {
                JSONObject jsonObject1 = new JSONObject();
                jsonObject1.put("DRINKSNAME", ShoppingCartActivity.listDrinksInReceipt.get(i).getDrinksName());
                jsonObject1.put("QUANTITY", ShoppingCartActivity.listDrinksInReceipt.get(i).getQuantity());
                jsonArray.put(jsonObject1);
            }
            jsonObject.put("LISTDRINKS", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (jsonObject != null) {
            requestQueue = Volley.newRequestQueue(PaymentReceiptActivity.this);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, urlServlet.getURLReceipt(), new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Toast.makeText(PaymentReceiptActivity.this, response, Toast.LENGTH_SHORT).show();
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(PaymentReceiptActivity.this, "Lỗi!", Toast.LENGTH_SHORT).show();
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> data = new HashMap<>();
                    data.put("Receipt", jsonObject.toString());
                    return data;
                }
            };
            requestQueue.add(stringRequest);
        }

    }

    private void getUserInfo() {
        requestQueue = Volley.newRequestQueue(PaymentReceiptActivity.this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, urlServlet.getURLUserInfoServlet(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    user = jsonToUser(jsonObject);

                    if (user.getGender().equals("M")) {
                        user.setGender("Nam");
                    } else user.setGender("Nữ");
                    textViewTenKhachHang.setText(user.getName());
                    textViewDiaChiGiaoHang.setText(user.getAddress());
                    textViewSoDienThoai.setText(user.getPhone());

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(PaymentReceiptActivity.this, "Lỗi!", Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> data = new HashMap<>();
                data.put("userName", username.trim());
                return data;
            }
        };
        requestQueue.add(stringRequest);
    }

    public static User jsonToUser(JSONObject jsonObject) {
        try {
            String name = jsonObject.getString("Name");
            String gender = jsonObject.getString("Gender");
            String phone = jsonObject.getString("Phone");
            String email = jsonObject.getString("Email");
            String address = jsonObject.getString("Address");

            return new User(name, gender, phone, email, address);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.itemTrangchu:
                Intent intent = new Intent(PaymentReceiptActivity.this,MainActivity.class);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.home_menu, menu);

        return true;
    }
}
