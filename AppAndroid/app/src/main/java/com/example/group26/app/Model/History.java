package com.example.group26.app.Model;

public class History {
    String date;
    String totalCost;
    String receiptsid;
    String address;
    String phoneNumber;

    public History(String date, String totalCost, String receiptsid, String address, String phoneNumber) {
        this.date = date;
        this.totalCost = totalCost;
        this.receiptsid = receiptsid;
        this.address = address;
        this.phoneNumber = phoneNumber;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(String totalCost) {
        this.totalCost = totalCost;
    }

    public String getReceiptsid() {
        return receiptsid;
    }

    public void setReceiptsid(String receiptsid) {
        this.receiptsid = receiptsid;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}
