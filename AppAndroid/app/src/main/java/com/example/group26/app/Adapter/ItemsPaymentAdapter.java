package com.example.group26.app.Adapter;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.BitmapFactory;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.group26.app.Activity.DataShoppingCart;
import com.example.group26.app.Model.Drinks;
import com.example.group26.app.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class ItemsPaymentAdapter extends BaseAdapter {
    ImageView ivDrinks;
    TextView tvDrinksName,tvPrice,textViewNumber;
    ArrayList<Drinks> listDrinks;
    Context context;
    SQLiteDatabase database;
    int number;

    public ItemsPaymentAdapter(Context context,ArrayList<Drinks> listDrinks) {
        this.listDrinks = listDrinks;
        this.context = context;
    }

    @Override
    public int getCount() {
        return listDrinks.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.items_payment,parent,false);

        ivDrinks = view.findViewById(R.id.ivDrinks);
        tvDrinksName = view.findViewById(R.id.tvDrinksName);
        tvPrice = view.findViewById(R.id.tvPrice);
        textViewNumber = view.findViewById(R.id.tvNumber);

        Drinks drinks=listDrinks.get(position);
        tvDrinksName.setText(drinks.getDrinksName());
        DecimalFormat decimalFormat=new DecimalFormat("###,###,###");
        tvPrice.setText("Giá : "+decimalFormat.format(drinks.getPrice())+"Đ");
        ivDrinks.setImageBitmap(BitmapFactory.decodeByteArray(drinks.getImageData(),0,drinks.getImageData().length));
        textViewNumber.setText(String.valueOf(getAmount(drinks)));

        return view;
    }

    private int getAmount(Drinks drinks){
        DataShoppingCart dataShoppingCart = new DataShoppingCart(context);
        database = dataShoppingCart.getWritableDatabase();
        String query = "SELECT * FROM "+DataShoppingCart.TB_SHOPPINGCART+" WHERE "+DataShoppingCart.TB_SHOPPINGCART_DRINKSID+
                " = ?";
        Cursor cursor = database.rawQuery(query, new String[]{drinks.getDrinksID()});
        cursor.moveToFirst();
        while(!cursor.isAfterLast()){
            number     = cursor.getInt(cursor.getColumnIndex(DataShoppingCart.TB_SHOPPINGCART_AMOUNT));
            cursor.moveToNext();
        }
        return number;
    }


    public static Drinks jsonToDrinks(JSONObject jsonObject) {
        try {
            String drinksID = jsonObject.getString("DrinksID");
            String drinksName = jsonObject.getString("DrinksName");
            Integer price = jsonObject.getInt("Price");
            String description=jsonObject.getString("Description");
            boolean state=jsonObject.getBoolean("State");
            byte[] imageData= Base64.decode(jsonObject.getString("Image"),Base64.DEFAULT);
            String type=jsonObject.getString("Type");
            Integer rate=jsonObject.getInt("Rate");
            return new Drinks(drinksID,drinksName,price,description,state,imageData,type,rate);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }
}
