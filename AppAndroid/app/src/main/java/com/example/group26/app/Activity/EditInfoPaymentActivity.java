package com.example.group26.app.Activity;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.group26.app.R;

public class EditInfoPaymentActivity extends AppCompatActivity{
    Button buttonHoanTat;
    CheckBox checkBoxDefaultAddress;
    TextInputLayout layoutHoVaTen,layoutDiaChi,layoutSoDienThoai;
    EditText editTextHoVaTen,editTextDiaChi,editTextSoDienThoai;
    public static String HoVaTen,DiaChi,SoDienThoai;
    MenuItem itemHome;
    public static String DiaChi2;
    String city,district;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            Drawable background = this.getResources().getDrawable(R.drawable.gradient_background);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(this.getResources().getColor(android.R.color.transparent));
            window.setBackgroundDrawable(background);

        }
        setContentView(R.layout.edit_info_payment);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarTimKiem);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        layoutHoVaTen = findViewById(R.id.tilHoVaTen);
        layoutDiaChi = findViewById(R.id.tilDiaChi);
        layoutSoDienThoai = findViewById(R.id.tilSoDienThoai);

        editTextHoVaTen = findViewById(R.id.edtHoVaTen);
        editTextDiaChi = findViewById(R.id.edtDiaChi);
        editTextSoDienThoai = findViewById(R.id.edtSoDienThoai);

        Intent intent = getIntent();
        editTextHoVaTen.setText(intent.getStringExtra("HoVaTen"));
        //editTextDiaChi.setText(intent.getStringExtra("DiaChi"));
        editTextSoDienThoai.setText(intent.getStringExtra("SoDienThoai"));

        Spinner spinnerDistrict = findViewById(R.id.spinnerDistrict);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.district_HCM, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerDistrict.setAdapter(adapter);
        district = "Quận Thủ Đức";
        spinnerDistrict.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                district = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        Spinner spinnerHCMCity = findViewById(R.id.spinnerHCMCity);
        ArrayAdapter<CharSequence> adapter2 = ArrayAdapter.createFromResource(this,
                R.array.HCM_City, android.R.layout.simple_spinner_item);
        adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerHCMCity.setAdapter(adapter2);

        city = "Thành phố Hồ Chí Minh.";

        DiaChi2 = district + ", " + city;

        editTextHoVaTen.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_LEFT = 0;
                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_BOTTOM = 3;

                if(event.getAction() == MotionEvent.ACTION_UP) {
                    if(event.getRawX() >= (editTextHoVaTen.getRight() - editTextHoVaTen.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        editTextHoVaTen.setText("");

                        return true;
                    }
                }
                return false;
            }
        });

        editTextDiaChi.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_LEFT = 0;
                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_BOTTOM = 3;

                if(event.getAction() == MotionEvent.ACTION_UP) {
                    if(event.getRawX() >= (editTextDiaChi.getRight() - editTextDiaChi.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        editTextDiaChi.setText("");

                        return true;
                    }
                }
                return false;
            }
        });

        editTextSoDienThoai.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_LEFT = 0;
                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_BOTTOM = 3;

                if(event.getAction() == MotionEvent.ACTION_UP) {
                    if(event.getRawX() >= (editTextSoDienThoai.getRight() - editTextSoDienThoai.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        editTextSoDienThoai.setText("");

                        return true;
                    }
                }
                return false;
            }
        });

        buttonHoanTat = findViewById(R.id.btnHoanTat);
        buttonHoanTat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HoVaTen = editTextHoVaTen.getText().toString();
                DiaChi = editTextDiaChi.getText().toString();
                SoDienThoai = editTextSoDienThoai.getText().toString();
                //Toast.makeText(EditInfoPaymentActivity.this, HoVaTen, Toast.LENGTH_SHORT).show();
                if(HoVaTen.isEmpty()){
                    layoutHoVaTen.setError("Mời bạn nhập thông tin!");
                } else if(DiaChi.isEmpty()){
                    layoutDiaChi.setError("Mời bạn nhập thông tin!");
                } else if(SoDienThoai.isEmpty()){
                    layoutSoDienThoai.setError("Mời bạn nhập thông tin!");
                } else {
                    layoutHoVaTen.setErrorEnabled(false);
                    layoutDiaChi.setErrorEnabled(false);
                    layoutSoDienThoai.setErrorEnabled(false);
                    MainActivity.checkUpdateInfo = true;
                    Intent intent = new Intent(EditInfoPaymentActivity.this,PaymentReceiptActivity.class);
                    startActivity(intent);

                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.itemTrangchu:
                Intent intent = new Intent(EditInfoPaymentActivity.this,MainActivity.class);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.home_menu, menu);
        itemHome = menu.findItem(R.id.itemTrangchu);

        return true;
    }
}
