package com.example.group26.app.Activity;

import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.group26.app.Adapter.ItemOffsetDecoration;
import com.example.group26.app.Adapter.SearchAdapter;
import com.example.group26.app.Model.Drinks;
import com.example.group26.app.Model.URLServlet;
import com.example.group26.app.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class SearchActivity extends AppCompatActivity implements SearchAdapter.ILoadMore {
    GridView gridView;
    MenuItem itemSearch,itemSearchView;
    SearchView searchView;
    TextView textView;

    ArrayList<Drinks> arrayList;
    RequestQueue requestQueue;
    RecyclerView recyclerView;
    URLServlet urlServlet;
    SearchAdapter searchAdapter;
    String key;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            Drawable background = this.getResources().getDrawable(R.drawable.gradient_background);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(this.getResources().getColor(android.R.color.transparent));
            //window.setNavigationBarColor(this.getResources().getColor(android.R.color.transparent));
            window.setBackgroundDrawable(background);
        }
        setContentView(R.layout.fragment_search);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarTimKiem);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //gridView = findViewById(R.id.gridviewSearch);
        textView = findViewById(R.id.tvSearch);

        recyclerView=(RecyclerView) findViewById(R.id.recyclerviewSearch);
        arrayList=new ArrayList<>();

        recyclerView.setLayoutManager(new GridLayoutManager(recyclerView.getContext(),3));

        int spacing = 10; // 50px
        boolean includeEdge = true;
        recyclerView.addItemDecoration(new ItemOffsetDecoration(1, spacing, includeEdge));
        searchAdapter=new SearchAdapter(recyclerView,recyclerView.getContext(),arrayList);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar_menu,menu);
        itemSearchView = menu.findItem(R.id.itSearchView);
        itemSearchView.setVisible(true);
        itemSearch = menu.findItem(R.id.itSearch);
        itemSearch.setVisible(false);
        searchView = (SearchView) itemSearchView.getActionView();
        ImageView icon = searchView.findViewById(android.support.v7.appcompat.R.id.search_button);
        icon.setColorFilter(Color.WHITE);

        searchView.setQueryHint("Tìm kiếm...");
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                urlServlet=new URLServlet();

                //Drinks
                loadMore(query);
                GetData(query);

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        return true;
    }

    private void GetData(String query) {

        requestQueue=Volley.newRequestQueue(recyclerView.getContext());
        StringRequest stringRequest = new StringRequest(Request.Method.POST, urlServlet.getURLSearchServlet(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    textView.setVisibility(View.GONE);
                    JSONArray jsonArray=new JSONArray(response);
                    for (int i=0;i<jsonArray.length();i++) {
                        JSONObject jsonObject= jsonArray.getJSONObject(i);
                        Drinks drinks = jsonToDrinks(jsonObject);
                        if (drinks != null) {
                            arrayList.add(drinks);
                        }
                        new AsyncTask<String, String, String>() {
                            @Override
                            protected String doInBackground(String... strings) {

                                return null;
                            }

                            @Override
                            protected void onProgressUpdate(String... values) {
                                super.onProgressUpdate(values);
                            }

                            @Override
                            protected void onPostExecute(String s) {
                                super.onPostExecute(s);
                                searchAdapter.notifyDataSetChanged();
                                searchAdapter.setLoaded();
                            }
                        }.execute();
                    }
                    if(arrayList.toString().equals("[]")){
                        textView.setVisibility(View.VISIBLE);
                        textView.setText("Không tìm thấy sản phẩm!");
                        textView.setTextSize(20);
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(SearchActivity.this, "Lỗi!", Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> data = new HashMap<>();
                data.put("DrinksName", query.toString().trim());
                data.put("index", String.valueOf(arrayList.size()));
                return data;
            }
        };
        requestQueue.add(stringRequest);
    }

    public static Drinks jsonToDrinks(JSONObject jsonObject) {
        try {
            String drinksID = jsonObject.getString("DrinksID");
            String drinksName = jsonObject.getString("DrinksName");
            Integer price = jsonObject.getInt("Price");
            String description=jsonObject.getString("Description");
            boolean state=jsonObject.getBoolean("State");
            byte[] imageData= Base64.decode(jsonObject.getString("Image"),Base64.DEFAULT);
            String type=jsonObject.getString("Type");
            Integer rate=jsonObject.getInt("Rate");
            return new Drinks(drinksID,drinksName,price,description,state,imageData,type,rate);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void onLoadMore() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                arrayList.add(null);
                searchAdapter.notifyItemInserted(arrayList.size()-1);
                arrayList.remove(arrayList.size()-1);
                searchAdapter.notifyItemRemoved(arrayList.size());
                GetData(key);
            }
        },1000);
    }

    private void loadMore(String query){
        if (query!=key){
            arrayList.clear();
        }
        key=query;
        searchAdapter.setLoadMore(this::onLoadMore);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(searchAdapter);
    }
}
