package com.example.group26.app.Fragment;

import android.graphics.drawable.AnimationDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.group26.app.Adapter.DrinksAdapter;
import com.example.group26.app.Adapter.ViewPagerHomeAdapter;
import com.example.group26.app.Model.Drinks;
import com.example.group26.app.Model.URLServlet;
import com.example.group26.app.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;


public class HomeFragment extends Fragment implements DrinksAdapter.ILoadMore{
    RequestQueue requestQueue;
    RecyclerView recyclerView;
    ArrayList<Drinks> arrayList;
    DrinksAdapter drinksAdapter;
    URLServlet urlServlet;
    ViewPager viewPager;
    private AnimationDrawable animationDrawable;
    private ImageView loading;

    int currentPage=0;
    Timer timer;
    final long DELAY_MS=500;
    final long PERIOD_MS = 3000;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        urlServlet=new URLServlet();

        View view=inflater.inflate(R.layout.fragment_home, container, false);

        loading=view.findViewById(R.id.loading);
        animationDrawable = (AnimationDrawable) loading.getDrawable();

        viewPager=(ViewPager) view.findViewById(R.id.viewPagerHome);

        //Drinks Hot
        recyclerView=(RecyclerView) view.findViewById(R.id.recyclerview);
        arrayList=new ArrayList<>();
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
        drinksAdapter=new DrinksAdapter(recyclerView,recyclerView.getContext(),arrayList);

        drinksAdapter.setLoadMore(this::onLoadMore);

        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(drinksAdapter);
        GetData();

        int NUM_PAGES=4;
        ViewPagerHomeAdapter adapter=new ViewPagerHomeAdapter(getActivity());
        viewPager.setAdapter(adapter);

        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == NUM_PAGES-1) {
                    currentPage = 0;
                }
                viewPager.setCurrentItem(currentPage++, true);
            }
        };

        timer = new Timer(); // This will create a new Thread
        timer .schedule(new TimerTask() { // task to be scheduled

            @Override
            public void run() {
                handler.post(Update);
            }
        }, DELAY_MS, PERIOD_MS);

        return view;
    }

    private static Drinks jsonToDrinks(JSONObject jsonObject) {
        try {
            String drinksID = jsonObject.getString("DrinksID");
            String drinksName = jsonObject.getString("DrinksName");
            Integer price = jsonObject.getInt("Price");
            String description=jsonObject.getString("Description");
            boolean state=jsonObject.getBoolean("State");
            byte[] imageData= Base64.decode(jsonObject.getString("Image"),Base64.DEFAULT);
            String type=jsonObject.getString("Type");
            Integer rate=2;
            return new Drinks(drinksID,drinksName,price,description,state,imageData,type,rate);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    private void GetData() {
        loading.setVisibility(View.VISIBLE);
        animationDrawable.start();
        requestQueue=Volley.newRequestQueue(recyclerView.getContext());
        StringRequest stringRequest = new StringRequest(Request.Method.POST, urlServlet.getURLHomeServlet(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONArray jsonArray=new JSONArray(response);
                    for (int i=0;i<jsonArray.length();i++) {
                        JSONObject jsonObject= jsonArray.getJSONObject(i);
                        Drinks drinks = jsonToDrinks(jsonObject);

                        if (drinks != null) {
                            arrayList.add(drinks);
                        }
                    }
                    new AsyncTask<String, String, String>() {
                        @Override
                        protected String doInBackground(String... strings) {

                            return null;
                        }

                        @Override
                        protected void onProgressUpdate(String... values) {
                            super.onProgressUpdate(values);
                        }

                        @Override
                        protected void onPostExecute(String s) {
                            super.onPostExecute(s);
                            drinksAdapter.notifyDataSetChanged();
                            drinksAdapter.setLoaded();
                            loading.setVisibility(View.GONE);
                            animationDrawable.stop();
                        }
                    }.execute();


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(recyclerView.getContext(), "Lỗi!", Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> data = new HashMap<>();
                data.put("index", String.valueOf(arrayList.size()));
                return data;
            }
        };
        requestQueue.add(stringRequest);
    }

    @Override
    public void onLoadMore() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                arrayList.add(null);
                drinksAdapter.notifyItemInserted(arrayList.size()-1);
                arrayList.remove(arrayList.size()-1);
                drinksAdapter.notifyItemRemoved(arrayList.size());
                GetData();
            }
        },1000);
    }
}
