package com.example.group26.app.Model;

public class DrinksInReceipt {
    String drinksName;
    int quantity;

    public DrinksInReceipt(String drinksName, int quantity) {
        this.drinksName = drinksName;
        this.quantity = quantity;
    }

    public String getDrinksName() {
        return drinksName;
    }

    public void setDrinksName(String drinksName) {
        this.drinksName = drinksName;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
