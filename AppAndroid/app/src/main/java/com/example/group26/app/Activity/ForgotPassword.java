package com.example.group26.app.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.group26.app.Adapter.UserInfoAdapter;
import com.example.group26.app.Model.URLServlet;
import com.example.group26.app.Model.User;
import com.example.group26.app.Model.UserInfo;
import com.example.group26.app.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ForgotPassword extends AppCompatActivity {
    Button btnSendEmail;
    EditText edtForgotPassword;
    URLServlet urlServlet=new URLServlet();
    ImageView imgBackLogin;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        btnSendEmail=findViewById(R.id.btnSendEmail);
        edtForgotPassword=findViewById(R.id.edtForgotPassword);

        btnSendEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
                String email = edtForgotPassword.getText().toString().trim();
                if(!email.matches(emailPattern)) {
                    Toast.makeText(ForgotPassword.this, "Email không hợp lệ", Toast.LENGTH_SHORT).show();
                }
                else sendEmail();
            }
        });

    }
    public void sendEmail() {
        RequestQueue requestQueue = Volley.newRequestQueue(ForgotPassword.this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, urlServlet.getURLForgotPassword(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                    Toast.makeText(ForgotPassword.this, response, Toast.LENGTH_SHORT).show();

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(ForgotPassword.this, "Lỗi!", Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> data = new HashMap<>();
                data.put("ForgotPassword",edtForgotPassword.getText().toString().trim());
                return data;
            }
        };
        requestQueue.add(stringRequest);
    }
}
