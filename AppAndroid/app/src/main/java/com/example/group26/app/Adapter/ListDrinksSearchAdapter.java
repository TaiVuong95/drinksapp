package com.example.group26.app.Adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.group26.app.Activity.DetailProductActivity;
import com.example.group26.app.Model.Drinks;
import com.example.group26.app.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class ListDrinksSearchAdapter extends BaseAdapter {
    Context context;
    ArrayList<Drinks> listDrinks;

    public ListDrinksSearchAdapter(Context context, int drinks, ArrayList<Drinks> listDrinks) {
        this.context = context;
        this.listDrinks = listDrinks;
    }



    @Override
    public int getCount() {
        return listDrinks.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    private class ViewHolder{
        ImageView ivDrinks;
        TextView tvDrinksName,tvPrice;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if(convertView == null){
            holder = new ViewHolder();

            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.drinks,parent,false);

            holder.ivDrinks=(ImageView) convertView.findViewById(R.id.ivDrinkss);
            holder.tvDrinksName=(TextView) convertView.findViewById(R.id.tvDrinksNames);
            holder.tvPrice=(TextView) convertView.findViewById(R.id.tvPrices);


            convertView.setTag(holder);

        }else{
            holder = (ViewHolder) convertView.getTag();
        }
        Drinks drinks=listDrinks.get(position);
        holder.tvDrinksName.setText(drinks.getDrinksName());
        DecimalFormat decimalFormat=new DecimalFormat("###,###,###");
        holder.tvPrice.setText("Giá : "+decimalFormat.format(drinks.getPrice())+"Đ");
        holder.ivDrinks.setImageBitmap(BitmapFactory.decodeByteArray(drinks.getImageData(),0,drinks.getImageData().length));
        holder.ivDrinks.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("Here1");
                Intent intent = new Intent(context,DetailProductActivity.class);
                Bundle bundle = new Bundle();
                System.out.println(drinks.getDrinksName()+drinks.getDescription());
                bundle.putSerializable("Drinks", drinks);
                intent.putExtra("Du lieu",bundle);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });

        return convertView;
    }

    public static Drinks jsonToDrinks(JSONObject jsonObject) {
        try {
            String drinksID = jsonObject.getString("DrinksID");
            String drinksName = jsonObject.getString("DrinksName");
            Integer price = jsonObject.getInt("Price");
            String description=jsonObject.getString("Description");
            boolean state=jsonObject.getBoolean("State");
            byte[] imageData= Base64.decode(jsonObject.getString("Image"),Base64.DEFAULT);
            String type=jsonObject.getString("Type");
            Integer rate=jsonObject.getInt("Rate");
            return new Drinks(drinksID,drinksName,price,description,state,imageData,type,rate);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }
}
