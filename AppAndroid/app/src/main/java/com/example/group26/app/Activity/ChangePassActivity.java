package com.example.group26.app.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.group26.app.Model.URLServlet;
import com.example.group26.app.R;

import java.util.HashMap;
import java.util.Map;

public class ChangePassActivity extends AppCompatActivity {
    Button btnSendPass;URLServlet urlServlet=new URLServlet();
    EditText edtNewPass;
    String username;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_pass);
        Intent intent=getIntent();
        username=intent.getStringExtra("username");
        btnSendPass=findViewById(R.id.btnSendPass);
        edtNewPass=findViewById(R.id.edtNewPass);
        btnSendPass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               if(edtNewPass.getText().toString().length()<8){
                    Toast.makeText(ChangePassActivity.this, "Mật khẩu ít nhất 8 ký tự", Toast.LENGTH_SHORT).show();
                }else {
                    sendDataChangePass();
                }
            }
        });
    }

    public void sendDataChangePass() {

        RequestQueue requestQueue = Volley.newRequestQueue(ChangePassActivity.this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, urlServlet.getURLChangePass(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                 Toast.makeText(ChangePassActivity.this, response, Toast.LENGTH_SHORT).show();

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(ChangePassActivity.this, "Lỗi!", Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> data = new HashMap<>();
                data.put("username", username.trim());
                data.put("password",edtNewPass.getText().toString().trim());
                return data;
            }
        };
        requestQueue.add(stringRequest);
    }
}
