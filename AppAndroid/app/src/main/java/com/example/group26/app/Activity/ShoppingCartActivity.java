package com.example.group26.app.Activity;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.group26.app.Adapter.ShoppingCartAdapter;
import com.example.group26.app.Model.Drinks;
import com.example.group26.app.Model.DrinksInReceipt;
import com.example.group26.app.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class ShoppingCartActivity extends AppCompatActivity implements UpdateShoppingCart {
    TextView textViewEmptyCart,textViewTongCong, textViewShoppingCart;
    public static TextView textViewTongGiaTien;
    Button buttonTienHanhThanhToan,buttonTiepTucMuaHang;
    RelativeLayout relativeLayoutTongCong;
    MenuItem itemSearch,itemShoppingCart,itemHome;
    View viewShoppingCart;
    public static int amountTotal;
    public static int totalCost;
    SQLiteDatabase database;
    public static ArrayList<Drinks> listDrinks;
    public static ArrayList<DrinksInReceipt> listDrinksInReceipt;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            Drawable background = this.getResources().getDrawable(R.drawable.gradient_background);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(this.getResources().getColor(android.R.color.transparent));
            window.setBackgroundDrawable(background);

        }
        setContentView(R.layout.fragment_shopping_cart);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarTimKiem);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        buttonTienHanhThanhToan = findViewById(R.id.btnTienHanhThanhToan);
        buttonTiepTucMuaHang = findViewById(R.id.btnTiepTucMuaHang);
        buttonTiepTucMuaHang.setVisibility(View.GONE);
        textViewEmptyCart = findViewById(R.id.tvEmptyCart);
        textViewEmptyCart.setVisibility(View.GONE);
        textViewTongCong = findViewById(R.id.tvTongCong);
        textViewTongGiaTien = findViewById(R.id.tvTongGiaTien);
        relativeLayoutTongCong = findViewById(R.id.llTongCong);


        if(getListDrinks(ShoppingCartActivity.this).size() == 0){
            textViewEmptyCart.setText("Không có sản phẩm nào trong giỏ hàng!");
            textViewEmptyCart.setTextSize(18);
            textViewEmptyCart.setVisibility(View.VISIBLE);
            buttonTiepTucMuaHang.setVisibility(View.VISIBLE);
            buttonTiepTucMuaHang.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(ShoppingCartActivity.this,MainActivity.class);
                    startActivity(intent);
                }
            });
            relativeLayoutTongCong.setVisibility(View.GONE);
            buttonTienHanhThanhToan.setVisibility(View.GONE);
        }else {
            textViewEmptyCart.setVisibility(View.GONE);
        }

        buttonTienHanhThanhToan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!MainActivity.checkLogin || MainActivity.checkLogout){
                    Intent intent = new Intent(ShoppingCartActivity.this,PaymentLoginSignUpActivity.class);
                    startActivity(intent);
                } else {
                    Intent intent = new Intent(ShoppingCartActivity.this,PaymentReceiptActivity.class);
                    startActivity(intent);
                }

            }
        });

        DecimalFormat decimalFormat=new DecimalFormat("###,###,###");
        textViewTongGiaTien.setText(decimalFormat.format(getTotalCost(ShoppingCartActivity.this))+"Đ");

        RecyclerView recyclerView=(RecyclerView) findViewById(R.id.recyclerShoppingCart);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager layoutManager=new LinearLayoutManager(ShoppingCartActivity.this,LinearLayoutManager.VERTICAL,false);
        recyclerView.setLayoutManager(layoutManager);

        ShoppingCartAdapter shoppingCartAdapter=new ShoppingCartAdapter(ShoppingCartActivity.this,getListDrinks(ShoppingCartActivity.this));
        recyclerView.setAdapter(shoppingCartAdapter);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.itemTrangchu:
                Intent intent = new Intent(ShoppingCartActivity.this,MainActivity.class);
                startActivity(intent);
                return true;
            case R.id.itSearch:
                Intent intent2 = new Intent(ShoppingCartActivity.this,SearchActivity.class);
                startActivity(intent2);
                return true;
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void openConnectSQL(Context context){
        DataShoppingCart dataShoppingCart = new DataShoppingCart(context);
        database = dataShoppingCart.getWritableDatabase();
    }

    public boolean addToCart(Drinks drinks, Context context, int amount){
        openConnectSQL(context);
        ContentValues contentValues = new ContentValues();
        contentValues.put(DataShoppingCart.TB_SHOPPINGCART_DRINKSID,drinks.getDrinksID());
        contentValues.put(DataShoppingCart.TB_SHOPPINGCART_DRINKSNAME,drinks.getDrinksName());
        contentValues.put(DataShoppingCart.TB_SHOPPINGCART_PRICE,drinks.getPrice());
        contentValues.put(DataShoppingCart.TB_SHOPPINGCART_DESCRIPTION,drinks.getDescription());
        contentValues.put(DataShoppingCart.TB_SHOPPINGCART_STATE,drinks.isState());
        contentValues.put(DataShoppingCart.TB_SHOPPINGCART_IMAGE,drinks.getImageData());
        contentValues.put(DataShoppingCart.TB_SHOPPINGCART_TYPE,drinks.getType());
        contentValues.put(DataShoppingCart.TB_SHOPPINGCART_AMOUNT,amount);
        contentValues.put(DataShoppingCart.TB_SHOPPINGCART_RATE,drinks.getRate());

        long id = database.insert(DataShoppingCart.TB_SHOPPINGCART,null,contentValues);
        if(id>0){
            return true;
        } else {
            return false;
        }
    }

       public ArrayList<Drinks> getListDrinks(Context context){
        openConnectSQL(context);
        listDrinks = new ArrayList<>();
        String query = "SELECT * FROM " + DataShoppingCart.TB_SHOPPINGCART;
        Cursor cursor = database.rawQuery(query,null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()){
            String drinksID = cursor.getString(cursor.getColumnIndex(DataShoppingCart.TB_SHOPPINGCART_DRINKSID));
            String drinksName = cursor.getString(cursor.getColumnIndex(DataShoppingCart.TB_SHOPPINGCART_DRINKSNAME));
            int price = cursor.getInt(cursor.getColumnIndex(DataShoppingCart.TB_SHOPPINGCART_PRICE));
            String description = cursor.getString(cursor.getColumnIndex(DataShoppingCart.TB_SHOPPINGCART_DESCRIPTION));
            Boolean state = cursor.getInt(cursor.getColumnIndex(DataShoppingCart.TB_SHOPPINGCART_STATE))>0;
            byte[] image = cursor.getBlob(cursor.getColumnIndex(DataShoppingCart.TB_SHOPPINGCART_IMAGE));
            String type = cursor.getString(cursor.getColumnIndex(DataShoppingCart.TB_SHOPPINGCART_TYPE));
            Integer rate= cursor.getInt(cursor.getColumnIndex(DataShoppingCart.TB_SHOPPINGCART_RATE));
            Drinks drinks = new Drinks(drinksID,drinksName,price,description,state,image,type,rate);

            listDrinks.add(drinks);
            cursor.moveToNext();
        }
        return listDrinks;
    }

    public void deleteItemsInCart(String[] drinksID, Context context){
        openConnectSQL(context);
        database.delete(DataShoppingCart.TB_SHOPPINGCART,DataShoppingCart.TB_SHOPPINGCART_DRINKSID + "= ?", drinksID);
    }

    public int getTotalCost(Context context){
        openConnectSQL(context);
        listDrinksInReceipt = new ArrayList<>();
        int cost = 0;
        String query = "SELECT * FROM " + DataShoppingCart.TB_SHOPPINGCART;
        Cursor cursor = database.rawQuery(query,null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()){
            int price = cursor.getInt(cursor.getColumnIndex(DataShoppingCart.TB_SHOPPINGCART_PRICE));
            int amount = cursor.getInt(cursor.getColumnIndex(DataShoppingCart.TB_SHOPPINGCART_AMOUNT));
            String drinksName = cursor.getString(cursor.getColumnIndex(DataShoppingCart.TB_SHOPPINGCART_DRINKSNAME));
            cost = cost + price*amount;

            DrinksInReceipt drinksInReceipt = new DrinksInReceipt(drinksName,amount);
            listDrinksInReceipt.add(drinksInReceipt);

            cursor.moveToNext();
        }
        totalCost = cost;

        return totalCost;

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar_menu, menu);
        itemSearch = menu.findItem(R.id.itSearch);
        itemHome = menu.findItem(R.id.itemTrangchu);

        itemShoppingCart = menu.findItem(R.id.itShopping);
        viewShoppingCart = MenuItemCompat.getActionView(itemShoppingCart);
        textViewShoppingCart = viewShoppingCart.findViewById(R.id.tvAmountItemsCart);
        openConnectSQL(ShoppingCartActivity.this);
        String query = "SELECT SUM("+DataShoppingCart.TB_SHOPPINGCART_AMOUNT+") FROM "+DataShoppingCart.TB_SHOPPINGCART;
        amountTotal = (int) DatabaseUtils.longForQuery(database,query,null);
        textViewShoppingCart.setText(String.valueOf(amountTotal));


        return true;
    }

    public static Drinks jsonToDrinks(JSONObject jsonObject) {
        try {
            String drinksID = jsonObject.getString("DrinksID");
            String drinksName = jsonObject.getString("DrinksName");
            Integer price = jsonObject.getInt("Price");

            String description=jsonObject.getString("Description");
            boolean state=jsonObject.getBoolean("State");
            byte[] imageData= Base64.decode(jsonObject.getString("Image"),Base64.DEFAULT);
            String type=jsonObject.getString("Type");
            Integer rate=jsonObject.getInt("Rate");
            return new Drinks(drinksID,drinksName,price,description,state,imageData,type,rate);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void updateItemCart(boolean check) {
        if (check){
            totalCost = 0;
            if(getListDrinks(ShoppingCartActivity.this).size() == 0){
                textViewEmptyCart.setText("Không có sản phẩm nào trong giỏ hàng!");
                textViewEmptyCart.setTextSize(18);
                textViewEmptyCart.setVisibility(View.VISIBLE);
                buttonTiepTucMuaHang.setVisibility(View.VISIBLE);
                buttonTiepTucMuaHang.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(ShoppingCartActivity.this,MainActivity.class);
                        startActivity(intent);
                    }
                });
                relativeLayoutTongCong.setVisibility(View.GONE);
                buttonTienHanhThanhToan.setVisibility(View.GONE);
            }else {
                textViewEmptyCart.setVisibility(View.GONE);
            }

            DecimalFormat decimalFormat=new DecimalFormat("###,###,###");
            textViewTongGiaTien.setText(decimalFormat.format(getTotalCost(ShoppingCartActivity.this))+"Đ");

            RecyclerView recyclerView=(RecyclerView) findViewById(R.id.recyclerShoppingCart);
            recyclerView.setHasFixedSize(true);
            LinearLayoutManager layoutManager=new LinearLayoutManager(ShoppingCartActivity.this,LinearLayoutManager.VERTICAL,false);
            recyclerView.setLayoutManager(layoutManager);

            ShoppingCartAdapter shoppingCartAdapter=new ShoppingCartAdapter(ShoppingCartActivity.this,getListDrinks(ShoppingCartActivity.this));
            recyclerView.setAdapter(shoppingCartAdapter);

            openConnectSQL(ShoppingCartActivity.this);

            String query = "SELECT SUM("+DataShoppingCart.TB_SHOPPINGCART_AMOUNT+") FROM "+DataShoppingCart.TB_SHOPPINGCART;
            amountTotal = (int) DatabaseUtils.longForQuery(database,query,null);
            textViewShoppingCart.setText(String.valueOf(amountTotal));
        }else {}

    }

}
