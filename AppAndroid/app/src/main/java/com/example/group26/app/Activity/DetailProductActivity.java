package com.example.group26.app.Activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.group26.app.Fragment.LoginSignUpFragment;
import com.example.group26.app.Model.Drinks;
import com.example.group26.app.Model.URLServlet;
import com.example.group26.app.R;
import com.willy.ratingbar.BaseRatingBar;
import com.willy.ratingbar.RotationRatingBar;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Console;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class DetailProductActivity extends AppCompatActivity{
    TextView txtTenSp,txtGiaSp,txtMTSP,txtSizeSP;
    ImageView imgHinhSp;
    Button btnAddToCart,btnRating;
    ImageButton btnPlus,btnMinus;
    RequestQueue requestQueue;
    URLServlet urlServlet;
    public static Boolean check=false;
    TextView textViewShoppingCart,txtDateTime,txtAmount;
    Drinks drinks;
    Bitmap bmp;
    ShoppingCartActivity shoppingCartActivity = new ShoppingCartActivity();
    boolean onPause = false;
    int[] SizeSP = {new Integer(1)};
    SQLiteDatabase database;
    int amountTotal;
    Toolbar toolbar;
    Animation slide_downd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            Drawable background = this.getResources().getDrawable(R.drawable.gradient_background);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(this.getResources().getColor(android.R.color.transparent));
            window.setBackgroundDrawable(background);
        }
        setContentView(R.layout.activity_detail_product);
        txtDateTime =(TextView)findViewById(R.id.datetime);
        txtTenSp    = (TextView)findViewById(R.id.txtTenSp);
        txtGiaSp    = (TextView)findViewById(R.id.txtGiaTien);
        txtMTSP     = (TextView)findViewById(R.id.txtMTSP);
        imgHinhSp   =(ImageView)findViewById(R.id.imgProduct);
        btnPlus     =(ImageButton)findViewById(R.id.plus);
        btnMinus    =(ImageButton)findViewById(R.id.minus);
        txtSizeSP   =(TextView)findViewById(R.id.sizeSP);
        btnAddToCart=(Button)findViewById(R.id.btnAddToCart);
        toolbar     = (Toolbar)findViewById(R.id.toolbar);
        txtAmount = (TextView) findViewById(R.id.tvAmountItemsCart);
        ImageView imgCart =(ImageView)findViewById(R.id.cart);
        slide_downd= AnimationUtils.loadAnimation(DetailProductActivity.this, R.anim.bounce);
        btnRating=findViewById(R.id.btnRating);
        final RotationRatingBar ratingBar=findViewById(R.id.ratingBar);

        //Toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String strDate = sdf.format(c.getTime());
        txtDateTime.setText(strDate);

        Intent intent = getIntent();
        Bundle bundle= intent.getBundleExtra("Du lieu");
        drinks = (Drinks) bundle.getSerializable("Drinks");
        bmp = BitmapFactory.decodeByteArray(drinks.getImageData(), 0, drinks.getImageData().length);
        txtTenSp.setText(drinks.getDrinksName());
        DecimalFormat decimalFormat=new DecimalFormat("###,###,###");
        txtGiaSp.setText(""+decimalFormat.format(drinks.getPrice()));
        imgHinhSp.setImageBitmap(bmp);
        String[] separated = drinks.getDescription().split("-");
        txtMTSP.setText(separated[0]);
        txtAmount.setText(String.valueOf(getAmoutTotal(amountTotal)));

        //Ratingbar
        ratingBar.setNumStars(5);
        if (drinks.getRate()!=-1){
            ratingBar.setRating(drinks.getRate());
        }else{
            ratingBar.setRating(0);
        }
        ratingBar.setStarPadding(10);
        ratingBar.setOnRatingChangeListener(new BaseRatingBar.OnRatingChangeListener() {
            @Override
            public void onRatingChange(BaseRatingBar baseRatingBar, float v) {
                ratingBar.setRating(v);
            }
        });
        FragmentManager fragmentManager=getSupportFragmentManager();
        FragmentTransaction fragmentTransaction=fragmentManager.beginTransaction();
        btnRating.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!MainActivity.checkLogin || MainActivity.checkLogout){
                    check=true;
                    if (fragmentManager.findFragmentByTag("LoginSignUpFragment") == null) {
                        fragmentTransaction.add(R.id.fragmentContainer, new LoginSignUpFragment(), "LoginSignUpFragment");
                        fragmentTransaction.addToBackStack("LoginSignUpFragment");
                        fragmentTransaction.commit();
                    } else {
                        fragmentManager.popBackStack("LoginSignUpFragment", 0);
                    }
                } else {
                    float rate = ratingBar.getRating();
                    sendRating(rate,MainActivity.username.trim());
                }
            }
        });


        imgCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DetailProductActivity.this,ShoppingCartActivity.class);
                startActivity(intent);
            }
        });

        btnPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SizeSP[0] = SizeSP[0] +1;
                txtSizeSP.startAnimation(slide_downd);
                txtSizeSP.setText(SizeSP[0]+"");
            }
        });
        btnMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SizeSP[0] = SizeSP[0] -1;
                if(SizeSP[0]>=1){
                    txtSizeSP.startAnimation(slide_downd);
                    txtSizeSP.setText(SizeSP[0]+"");
                }
                else{SizeSP[0]=1;}
            }
        });
        btnAddToCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(DetailProductActivity.this, String.valueOf(SizeSP[0]), Toast.LENGTH_SHORT).show();
                boolean check = shoppingCartActivity.addToCart(drinks,DetailProductActivity.this,SizeSP[0]);
                if(check){
                    Toast.makeText(DetailProductActivity.this, "Đã thêm sản phẩm vào giỏ hàng thành công!", Toast.LENGTH_SHORT).show();
                    //getAmoutTotal(amountTotal);
                    txtAmount.setText(String.valueOf(getAmoutTotal(amountTotal)));
                } else {
                    Toast.makeText(DetailProductActivity.this, "Sản phẩm đã có trong giỏ hàng!", Toast.LENGTH_SHORT).show();
                }

            }
        });

    }

    private int getAmoutTotal(int amountTotal){
        DataShoppingCart dataShoppingCart = new DataShoppingCart(DetailProductActivity.this);
        database = dataShoppingCart.getWritableDatabase();
        String query = "SELECT * FROM " + DataShoppingCart.TB_SHOPPINGCART;
        Cursor cursor = database.rawQuery(query,null);
        cursor.moveToFirst();

        while (!cursor.isAfterLast()){
            int amount = cursor.getInt(cursor.getColumnIndex(DataShoppingCart.TB_SHOPPINGCART_AMOUNT));
            amountTotal = amountTotal + amount;

            cursor.moveToNext();
        }
        return amountTotal;
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        View convertView = getLayoutInflater().inflate(R.layout.toolbar,null);
////        getMenuInflater().inflate(R.menu.toolbar_menu,menu);
////        MenuItem itemShoppingCart = menu.findItem(R.id.itShopping);
////        View viewShoppingCart = MenuItemCompat.getActionView(itemShoppingCart);
////        textViewShoppingCart = viewShoppingCart.findViewById(R.id.tvAmountItemsCart);
////        //amountTotal = getAmoutTotal();
////        textViewShoppingCart.setText(String.valueOf(getAmoutTotal(amountTotal)));
////
////        viewShoppingCart.setOnClickListener(new View.OnClickListener() {
////            @Override
////            public void onClick(View v) {
////                Intent intent = new Intent(DetailProductActivity.this,ShoppingCartActivity.class);
////                startActivity(intent);
////            }
////        });
//
//        return true;
//    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public static Drinks jsonToDrinks(JSONObject jsonObject) {
        try {
            String drinksID = jsonObject.getString("DrinksID");
            String drinksName = jsonObject.getString("DrinksName");
            Integer price = jsonObject.getInt("Price");
            String description=jsonObject.getString("Description");
            boolean state=jsonObject.getBoolean("State");
            byte[] imageData= Base64.decode(jsonObject.getString("Image"),Base64.DEFAULT);
            String type=jsonObject.getString("Type");
            Integer rate=jsonObject.getInt("Rate");
            return new Drinks(drinksID,drinksName,price,description,state,imageData,type,rate);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(onPause){
            //ShoppingCartActivity shoppingCartActivity = new ShoppingCartActivity();
            // amountTotal = getAmoutTotal();
            txtAmount.setText(String.valueOf(ShoppingCartActivity.amountTotal));
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        onPause = true;
    }

    public void sendRating(Float rating,String userName){
        urlServlet=new URLServlet();
        requestQueue = Volley.newRequestQueue(DetailProductActivity.this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, urlServlet.getURLRating(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Toast.makeText(DetailProductActivity.this, response, Toast.LENGTH_SHORT).show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(DetailProductActivity.this, "Lỗi!", Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> data = new HashMap<>();
                data.put("Rate", rating.toString());
                data.put("DrinksID",drinks.getDrinksID());
                data.put("Username",userName);
                return data;
            }
        };
        requestQueue.add(stringRequest);
    }
}
