package com.example.group26.app.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.group26.app.Adapter.HistoryInfoAdapter;
import com.example.group26.app.Adapter.UserInfoAdapter;
import com.example.group26.app.Model.Drinks;
import com.example.group26.app.Model.History;
import com.example.group26.app.Model.HistoryShoppingInfo;
import com.example.group26.app.Model.Listdrinks;
import com.example.group26.app.Model.URLServlet;
import com.example.group26.app.Model.User;
import com.example.group26.app.Model.UserInfo;
import com.example.group26.app.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class HistoryShopping extends AppCompatActivity {
    ImageView imgbtnBack;String username; URLServlet urlServlet=new URLServlet();
    History history;Listdrinks listdrinks;ListView lvHistoryInfo;
    ArrayList<HistoryShoppingInfo> historyArrayList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history_shopping);
       // Toast.makeText(this, "Hello", Toast.LENGTH_SHORT).show();
        lvHistoryInfo=findViewById(R.id.lvHistoryInfo);
        historyArrayList=new ArrayList<>();
               BackUserInfo();
        Intent intent=getIntent();
        username=intent.getStringExtra("username");
      //  Toast.makeText(this, username, Toast.LENGTH_SHORT).show();
        sendDataHistoryShopping();
    }
    public void sendDataHistoryShopping() {
        RequestQueue requestQueue = Volley.newRequestQueue(HistoryShopping.this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, urlServlet.getURLHistoryShoppingServlet(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
             //  Toast.makeText(HistoryShopping.this, response, Toast.LENGTH_SHORT).show();
                try {
                    JSONArray jsonArray=new JSONArray(response);
                    for (int i=0;i<jsonArray.length();i++) {
                        if(i>=1){
                            historyArrayList.add(new HistoryShoppingInfo("",""));
                        }
                        JSONObject jsonObject= jsonArray.getJSONObject(i);
                        history= jsonToHistory(jsonObject);
                        historyArrayList.add(new HistoryShoppingInfo("Ngày",history.getDate()));
                        historyArrayList.add(new HistoryShoppingInfo("Số điện thoại",history.getPhoneNumber()));
                        historyArrayList.add(new HistoryShoppingInfo("Địa chỉ",history.getAddress()));
                        historyArrayList.add(new HistoryShoppingInfo("Tên đồ uống","Thành tiền"));
                        JSONArray jsonList=jsonArray.getJSONObject(i).getJSONArray("LISTDRINKS");
                        for (int j=0;j<jsonList.length();j++) {
                            JSONObject object=jsonList.getJSONObject(j);
                            listdrinks=jsonToDrinks(object);
                            historyArrayList.add(new HistoryShoppingInfo(listdrinks.getDrinksName()
                                    ,listdrinks.getQuantity()+"*"+listdrinks.getPrice()));

                        }
                        historyArrayList.add(new HistoryShoppingInfo("Tổng tiền",history.getTotalCost()+" Đồng"));


//                        Toast.makeText(HistoryShopping.this, history.getAddress(), Toast.LENGTH_SHORT).show();
//                        Toast.makeText(HistoryShopping.this, listdrinks.getDrinksName(), Toast.LENGTH_SHORT).show();
                    //   JSONArray jsonListdrinks=jsonArray.getJSONArray()
                    }
                    HistoryInfoAdapter adapter=new HistoryInfoAdapter(HistoryShopping.this,R.layout.historyshoppinginfo,historyArrayList);
                    lvHistoryInfo.setAdapter(adapter);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(HistoryShopping.this, "Lỗi!", Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> data = new HashMap<>();
                data.put("Username",username.trim() );
                return data;
            }
        };
        requestQueue.add(stringRequest);
    }

    private void BackUserInfo() {

        imgbtnBack=findViewById(R.id.imgbtnBack);
        imgbtnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
    public static History jsonToHistory(JSONObject jsonObject){
        try {
            String date = jsonObject.getString("DATE");
            String total_cost=jsonObject.getString("TOTAL_COST");
            String receiptsid = jsonObject.getString("RECEIPTSID");
            String address = jsonObject.getString("ADDRESS");
            String phone_number=jsonObject.getString("PHONE_NUMBER");

            return new History(date,total_cost,receiptsid,address,phone_number);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return  null;
    }
    public static Listdrinks jsonToDrinks(JSONObject jsonObject){
        try {
            String quantity = jsonObject.getString("QUANTITY");
            String drinksname=jsonObject.getString("DRINKSNAME");
            String price=jsonObject.getString("PRICE");
            return new Listdrinks(drinksname,quantity,price);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return  null;
    }
}
