package com.example.group26.app.Activity;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DataShoppingCart extends SQLiteOpenHelper {
    public static String TB_SHOPPINGCART = "SHOPPINGCART";
    public static String TB_SHOPPINGCART_DRINKSID = "DRINKSID";
    public static String TB_SHOPPINGCART_DRINKSNAME = "DRINKSNAME";
    public static String TB_SHOPPINGCART_PRICE = "PRICE";
    public static String TB_SHOPPINGCART_DESCRIPTION = "DESCRIPTION";
    public static String TB_SHOPPINGCART_STATE = "STATE";
    public static String TB_SHOPPINGCART_IMAGE = "IMAGE";
    public static String TB_SHOPPINGCART_TYPE = "TYPE";
    public static String TB_SHOPPINGCART_AMOUNT = "AMOUNT";
    public static String TB_SHOPPINGCART_RATE = "RATE";


    public DataShoppingCart(Context context) {
        super(context, "SQLSHOPPINGCART", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String tableShoppingCart = "CREATE TABLE "+TB_SHOPPINGCART+" ("+TB_SHOPPINGCART_DRINKSID+" TEXT PRIMARY KEY, "+TB_SHOPPINGCART_DRINKSNAME+" TEXT," +
                " "+TB_SHOPPINGCART_PRICE+" REAL, "+TB_SHOPPINGCART_DESCRIPTION+" TEXT , "+TB_SHOPPINGCART_STATE+" BOOLEAN " +
                ","+TB_SHOPPINGCART_IMAGE+" BLOB, "+TB_SHOPPINGCART_TYPE+" TEXT, "+TB_SHOPPINGCART_AMOUNT+" INTEGER,"+TB_SHOPPINGCART_RATE+" INTEGER);";


        db.execSQL(tableShoppingCart);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
