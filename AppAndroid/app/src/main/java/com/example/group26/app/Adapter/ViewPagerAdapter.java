package com.example.group26.app.Adapter;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.group26.app.Fragment.FragmentProduct;

import java.util.ArrayList;
import java.util.List;

public class ViewPagerAdapter extends FragmentPagerAdapter {
    List<Fragment> listFragment = new ArrayList<Fragment>();
    List<String> titleFragment = new ArrayList<String>();

    public ViewPagerAdapter(FragmentManager fm) {
        super(fm);

        listFragment.add(new FragmentProduct());
        listFragment.add(new FragmentProduct());
        listFragment.add(new FragmentProduct());
        listFragment.add(new FragmentProduct());
        listFragment.add(new FragmentProduct());
        listFragment.add(new FragmentProduct());
        listFragment.add(new FragmentProduct());
        listFragment.add(new FragmentProduct());
        listFragment.add(new FragmentProduct());
        listFragment.add(new FragmentProduct());
        listFragment.add(new FragmentProduct());

        titleFragment.add("Cà phê");
        titleFragment.add("Trà sủi bọt");
        titleFragment.add("Trà con gái");
        titleFragment.add("Trà bí đao");
        titleFragment.add("Trà sữa");
        titleFragment.add("Trà tươi 2");
        titleFragment.add("Trà đặc biệt");
        titleFragment.add("Kem tuyết");
        titleFragment.add("Nước ép");
        titleFragment.add("Sinh tố");
        titleFragment.add("Nước ngọt");

    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0: // Fragment # 0 - This will show FirstFragment
                return FragmentProduct.newInstance(0, "1");
            case 1: // Fragment # 0 - This will show FirstFragment different title
                return FragmentProduct.newInstance(1, "2");
            case 2: // Fragment # 1 - This will show SecondFragment
                return FragmentProduct.newInstance(2, "3");
            case 3: // Fragment # 0 - This will show FirstFragment different title
                return FragmentProduct.newInstance(3, "4");
            case 4: // Fragment # 0 - This will show FirstFragment different title
                return FragmentProduct.newInstance(4, "5");
            case 5: // Fragment # 0 - This will show FirstFragment different title
                return FragmentProduct.newInstance(5, "6");
            case 6: // Fragment # 0 - This will show FirstFragment different title
                return FragmentProduct.newInstance(6, "7");
            case 7: // Fragment # 0 - This will show FirstFragment different title
                return FragmentProduct.newInstance(6, "8");
            case 8: // Fragment # 0 - This will show FirstFragment different title
                return FragmentProduct.newInstance(6, "9");
            case 9: // Fragment # 0 - This will show FirstFragment different title
                return FragmentProduct.newInstance(6, "10");
            default:
                return FragmentProduct.newInstance(7, "11");
        }
    }

    @Override
    public int getCount() {
        return listFragment.size();
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return titleFragment.get(position);
    }
}
