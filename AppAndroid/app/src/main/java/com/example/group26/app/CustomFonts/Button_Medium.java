package com.example.group26.app.CustomFonts;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.Button;

public class Button_Medium extends android.support.v7.widget.AppCompatButton{
    public Button_Medium(Context context) {
        super(context);
        init();
    }

    public Button_Medium(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public Button_Medium(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        if (!isInEditMode()) {
            Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "myfonts/Lato-Regular.ttf");
            setTypeface(tf);
        }
    }
}
