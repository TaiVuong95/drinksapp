package com.example.group26.app.Adapter;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.group26.app.Activity.DataShoppingCart;
import com.example.group26.app.Activity.DetailProductActivity;
import com.example.group26.app.Activity.ShoppingCartActivity;
import com.example.group26.app.Activity.UpdateShoppingCart;
import com.example.group26.app.Model.Drinks;
import com.example.group26.app.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class ShoppingCartAdapter extends RecyclerView.Adapter<ShoppingCartAdapter.ItemHolder> {
    Context context;
    ArrayList<Drinks> listDrinks;
    SQLiteDatabase database;
    int amount;
    Animation slide_downd;
    ShoppingCartActivity shoppingCartActivity = new ShoppingCartActivity();
    UpdateShoppingCart updateItemCart;
    int[] amountDrinks = {new Integer(1)};

    public ShoppingCartAdapter(Context context, ArrayList<Drinks> listDrinks) {
        this.context = context;
        this.listDrinks = listDrinks;

    }

    @NonNull
    @Override
    public ItemHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v= LayoutInflater.from(parent.getContext()).inflate(R.layout.drinks_shopping_cart,parent,false);
        return new ItemHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemHolder holder, int position) {

        Drinks drinks=listDrinks.get(position);
        holder.tvDrinksName.setText(drinks.getDrinksName());
        DecimalFormat decimalFormat=new DecimalFormat("###,###,###");
        holder.tvPrice.setText("Giá : "+decimalFormat.format(drinks.getPrice())+"Đ");
        holder.ivDrinks.setImageBitmap(BitmapFactory.decodeByteArray(drinks.getImageData(),0,drinks.getImageData().length));
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context,DetailProductActivity.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable("Drinks", drinks);
                intent.putExtra("Du lieu",bundle);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });



        amount = getAmount(drinks);
        holder.textViewAmount.setText(amount+"");
        if(amount==1){
            holder.imageViewMinus.setVisibility(View.INVISIBLE);
        }

        holder.btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shoppingCartActivity.deleteItemsInCart(new String[]{drinks.getDrinksID()},context);

                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setCancelable(false);
                builder.setMessage("Bạn muốn xóa sản phẩm?");
                builder.setPositiveButton("Đồng ý", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        listDrinks.remove(position);
                        updateItemCart.updateItemCart(true);
                        dialog.cancel();
                    }
                });
                builder.setNegativeButton("Hủy", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                builder.show();

            }
        });

        slide_downd= AnimationUtils.loadAnimation(context, R.anim.bounce);

        holder.imageViewPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.imageViewMinus.setVisibility(View.VISIBLE);
                amount = getAmount(drinks);
                amountDrinks[0]=amount;
                amountDrinks[0] = amountDrinks[0] +1;
                ContentValues contentValues = new ContentValues();
                contentValues.put(DataShoppingCart.TB_SHOPPINGCART_AMOUNT,amountDrinks[0]);
                database.update(DataShoppingCart.TB_SHOPPINGCART, contentValues, DataShoppingCart.TB_SHOPPINGCART_DRINKSID+
                        " = ?", new String[]{drinks.getDrinksID()});
                holder.textViewAmount.startAnimation(slide_downd);
                holder.textViewAmount.setText(String.valueOf(amountDrinks[0]));
                updateItemCart.updateItemCart(true);

            }
        });
        holder.imageViewMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                amount = getAmount(drinks);
                amountDrinks[0] = amount;
                if(amountDrinks[0]==2){
                    holder.imageViewMinus.setVisibility(View.INVISIBLE);
                }
                if(amountDrinks[0]>1){
                    amountDrinks[0]--;
                    ContentValues contentValues = new ContentValues();
                    contentValues.put(DataShoppingCart.TB_SHOPPINGCART_AMOUNT,amountDrinks[0]);
                    database.update(DataShoppingCart.TB_SHOPPINGCART, contentValues, DataShoppingCart.TB_SHOPPINGCART_DRINKSID+
                            " = ?", new String[]{drinks.getDrinksID()});
                    holder.textViewAmount.startAnimation(slide_downd);
                    holder.textViewAmount.setText(String.valueOf(amountDrinks[0]));
                    updateItemCart.updateItemCart(true);
                }

            }
        });
    }



    private int getAmount(Drinks drinks){
        DataShoppingCart dataShoppingCart = new DataShoppingCart(context);
        database = dataShoppingCart.getWritableDatabase();
        String query = "SELECT * FROM "+DataShoppingCart.TB_SHOPPINGCART+" WHERE "+DataShoppingCart.TB_SHOPPINGCART_DRINKSID+
                " = ?";
        Cursor cursor = database.rawQuery(query, new String[]{drinks.getDrinksID()});
        cursor.moveToFirst();
        while(!cursor.isAfterLast()){
            amount     = cursor.getInt(cursor.getColumnIndex(DataShoppingCart.TB_SHOPPINGCART_AMOUNT));
            cursor.moveToNext();
        }
        return amount;
    }

    @Override
    public int getItemCount() {
        return listDrinks==null ? 0 : listDrinks.size();
    }

    public class ItemHolder extends RecyclerView.ViewHolder{
        public ImageView ivDrinks,imageViewPlus,imageViewMinus;
        public TextView tvDrinksName,tvPrice,textViewAmount;
        public ImageView btnDelete;



        public ItemHolder(View itemView) {
            super(itemView);
            ivDrinks=(ImageView) itemView.findViewById(R.id.ivDrinks);
            tvDrinksName=(TextView) itemView.findViewById(R.id.tvDrinksName);
            tvPrice=(TextView) itemView.findViewById(R.id.tvPrice);
            btnDelete = itemView.findViewById(R.id.btnDelete);
            imageViewMinus = itemView.findViewById(R.id.ivMinus);
            imageViewPlus = itemView.findViewById(R.id.ivPlus);
            textViewAmount = itemView.findViewById(R.id.amountDrinks);
            updateItemCart = (UpdateShoppingCart) itemView.getContext();

        }
    }

    public static Drinks jsonToDrinks(JSONObject jsonObject) {
        try {
            String drinksID = jsonObject.getString("DrinksID");
            String drinksName = jsonObject.getString("DrinksName");
            Integer price = jsonObject.getInt("Price");
            String description=jsonObject.getString("Description");
            boolean state=jsonObject.getBoolean("State");
            byte[] imageData= Base64.decode(jsonObject.getString("Image"),Base64.DEFAULT);
            String type=jsonObject.getString("Type");
            Integer rate=jsonObject.getInt("Rate");
            return new Drinks(drinksID,drinksName,price,description,state,imageData,type,rate);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }
}
