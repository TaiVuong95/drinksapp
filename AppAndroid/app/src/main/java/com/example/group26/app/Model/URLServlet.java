package com.example.group26.app.Model;

public class URLServlet {
    public String IP= "http://192.168.1.102:8080";
          //  "http://cnpmgroup26.jelastic.servint.net/";
   // public String IP="http://192.168.42.101:8080";
//In order to use Instant Run with this device running API 25, you must install platform API 25 in your SDK
    public URLServlet() {
    }
    //Home
    public String getURLHomeServlet(){
        return IP + "/DrinksServlet";
    }
    //Search
    public String getURLSearchServlet(){
        return IP + "/SearchServlet";
    }
    //SignIn
    public String getURLLogoinServlet(){
        return IP + "/LoginServlet";
    }
    //SignUp
    public String getURLSignUpServlet(){
        return IP + "/SignUpServlet";
    }
    //UserInfo
    public String getURLUserInfoServlet(){return IP + "/UserInfoServlet";}
    //Category
    public String getURLCategoryServlet(){return IP + "/CategoryDrinksServlet";}
    //HistoryShopping
    public String getURLHistoryShoppingServlet(){
        return IP + "/HistoryShopping";
    }
    public String getURLReceipt(){ return IP + "/addReceipt";}
    public String getURLForgotPassword(){
        return IP + "/ForgotPassword";
    }
    public String getURLRating(){return IP+"/Rating";}
    public String getURLChangePass(){return IP+"/ChangePass";}
}
