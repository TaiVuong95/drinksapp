package com.example.group26.app.Activity;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

import com.example.group26.app.Fragment.LoginSignUpFragment;
import com.example.group26.app.R;

public class PaymentLoginSignUpActivity extends AppCompatActivity {
    Button buttonDangNhapDangKy;
    public static Boolean check = false;
    MenuItem itemHome;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            Drawable background = this.getResources().getDrawable(R.drawable.gradient_background);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(this.getResources().getColor(android.R.color.transparent));
            window.setBackgroundDrawable(background);

        }
        setContentView(R.layout.payment_login_signup);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarTimKiem);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);



        buttonDangNhapDangKy = findViewById(R.id.btnDangNhapDangKy);
        FragmentManager fragmentManager=getSupportFragmentManager();
        FragmentTransaction fragmentTransaction=fragmentManager.beginTransaction();
        buttonDangNhapDangKy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonDangNhapDangKy.setVisibility(View.GONE);
                check = true;
                if(MainActivity.checkLogin==false || MainActivity.checkLogout) {
                    if (fragmentManager.findFragmentByTag("LoginSignUpFragment") == null) {
                        fragmentTransaction.replace(R.id.fragmentContainer, new LoginSignUpFragment(), "LoginSignUpFragment");
                        fragmentTransaction.addToBackStack("LoginSignUpFragment");
                        fragmentTransaction.commit();
                    } else {
                        fragmentManager.popBackStack("LoginSignUpFragment", 0);
                    }
                }
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.itemTrangchu:
                Intent intent2 = new Intent(PaymentLoginSignUpActivity.this,MainActivity.class);
                startActivity(intent2);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.home_menu, menu);
        itemHome = menu.findItem(R.id.itemTrangchu);

        return true;
    }
}
