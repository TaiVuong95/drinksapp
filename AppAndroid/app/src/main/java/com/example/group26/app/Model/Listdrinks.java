package com.example.group26.app.Model;

public class Listdrinks {
    String quantity;
    String drinksName;
    String price;

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getDrinksName() {
        return drinksName;
    }

    public void setDrinksName(String drinksName) {
        this.drinksName = drinksName;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public Listdrinks( String drinksName,String quantity, String price) {
        this.quantity = quantity;
        this.drinksName = drinksName;
        this.price = price;
    }
}
