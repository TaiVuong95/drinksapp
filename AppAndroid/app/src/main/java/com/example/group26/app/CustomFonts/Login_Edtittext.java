package com.example.group26.app.CustomFonts;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.EditText;

public class Login_Edtittext extends android.support.v7.widget.AppCompatEditText {
    public Login_Edtittext(Context context) {
        super(context);
        init();
    }

    public Login_Edtittext(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public Login_Edtittext(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        if (!isInEditMode()) {
            Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "myfonts/Lato-Black.ttf");
            setTypeface(tf);
        }
    }
}
