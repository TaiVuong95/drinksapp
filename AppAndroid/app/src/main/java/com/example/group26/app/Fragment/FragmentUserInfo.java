package com.example.group26.app.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.group26.app.Activity.ChangePassActivity;
import com.example.group26.app.Activity.HistoryShopping;
import com.example.group26.app.Activity.MainActivity;
import com.example.group26.app.Adapter.UserInfoAdapter;
import com.example.group26.app.Model.URLServlet;
import com.example.group26.app.Model.User;
import com.example.group26.app.Model.UserInfo;
import com.example.group26.app.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class FragmentUserInfo extends Fragment {

    URLServlet urlServlet=new URLServlet();
    String username;ListView lvUserInfo;
    Boolean checkLogout=false;
    ArrayList<UserInfo>userArrayList;
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_user_info, container, false);
        Bundle bundle = this.getArguments();
        username = bundle.getString("username");
        lvUserInfo=view.findViewById(R.id.lvUserInfo);
//        Toast.makeText(getActivity(), username+"thành công", Toast.LENGTH_SHORT).show();//test
          sendDataUserInfo();
        return view;
    }
    public void sendDataUserInfo() {
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        StringRequest stringRequest = new StringRequest(Request.Method.POST, urlServlet.getURLUserInfoServlet(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
               //    Toast.makeText(getActivity(), response, Toast.LENGTH_SHORT).show();
                try {
                    JSONObject jsonObject=new JSONObject(response);
                    User user=jsonToUser(jsonObject);
              //      Toast.makeText(getActivity(), user.getName()+user.getGender()+user.getPhone()+user.getEmail()+user.getAddress(), Toast.LENGTH_SHORT).show();

                    if(user.getGender().equals("M")){
                        user.setGender("Nam");
                    }
                    else user.setGender("Nữ");
                   userArrayList =new ArrayList<UserInfo>();
                    userArrayList.add(new UserInfo(R.drawable.info_user,user.getName()));
                    userArrayList.add(new UserInfo(R.drawable.info_phone,user.getPhone()));
                    userArrayList.add(new UserInfo(R.drawable.info_gender,user.getGender()));
                    userArrayList.add(new UserInfo(R.drawable.info_email,user.getEmail()));
                    userArrayList.add(new UserInfo(R.drawable.info_address,user.getAddress()));
                    userArrayList.add(new UserInfo(R.drawable.info_shopping,"Lịch sử mua hàng"));
                    userArrayList.add(new UserInfo(R.drawable.img_key,"Thay đổi mật khẩu"));
                    userArrayList.add(new UserInfo(R.drawable.info_logout,"Đăng xuất"));
                    UserInfoAdapter adapter=new UserInfoAdapter(getActivity(),R.layout.user_info,userArrayList);
                    lvUserInfo.setAdapter(adapter);
                    lvUserInfo.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                         //   Toast.makeText(getActivity(),userArrayList.get(position).name, Toast.LENGTH_SHORT).show();
                            if(userArrayList.get(position).info.toString()=="Đăng xuất")
                            {
                                checkLogout=true;
                                MainActivity.checkLogout = true;
                                Toast.makeText(getActivity(), "Đăng xuất thành công", Toast.LENGTH_SHORT).show();
                                Intent intent=new Intent(getActivity(), MainActivity.class);
                                intent.putExtra("logout",checkLogout);
                                startActivity(intent);
                            }
                            if(userArrayList.get(position).info.toString()=="Lịch sử mua hàng"){
                              Intent intent =new Intent(getActivity(), HistoryShopping.class);
                              intent.putExtra("username",username);
                              startActivity(intent);
                            }
                            if(userArrayList.get(position).info.toString()=="Thay đổi mật khẩu"){
                                Intent intent =new Intent(getActivity(), ChangePassActivity.class);
                                intent.putExtra("username",username);
                                startActivity(intent);
                            }
                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getActivity(), "Lỗi!", Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> data = new HashMap<>();
                data.put("userName",username.trim() );
                return data;
            }
        };
        requestQueue.add(stringRequest);
    }
    public static User jsonToUser(JSONObject jsonObject) {
        try {
            String name = jsonObject.getString("Name");
            String gender=jsonObject.getString("Gender");
            String phone = jsonObject.getString("Phone");
            String email = jsonObject.getString("Email");
            String address=jsonObject.getString("Address");

            return new User(name,gender,phone,email,address);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

}
