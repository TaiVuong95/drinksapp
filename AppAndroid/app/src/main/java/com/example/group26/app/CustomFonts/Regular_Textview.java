package com.example.group26.app.CustomFonts;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

public class Regular_Textview extends android.support.v7.widget.AppCompatTextView {
    public Regular_Textview(Context context) {
        super(context);
        init();
    }

    public Regular_Textview(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public Regular_Textview(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        if (!isInEditMode()) {
            Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "myfonts/Roboto-Regular.ttf");
            setTypeface(tf);
        }
    }
}
