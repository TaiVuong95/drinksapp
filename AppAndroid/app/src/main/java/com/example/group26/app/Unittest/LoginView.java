package com.example.group26.app.Unittest;

public interface LoginView {
  String getUsername();
  String getPassword();
}
