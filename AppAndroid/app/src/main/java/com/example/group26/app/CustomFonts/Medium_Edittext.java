package com.example.group26.app.CustomFonts;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.EditText;

public class Medium_Edittext extends android.support.v7.widget.AppCompatEditText{
    public Medium_Edittext(Context context) {
        super(context);
        init();
    }

    public Medium_Edittext(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public Medium_Edittext(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }
    private void init() {
        if (!isInEditMode()) {
            Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "myfonts/Roboto-Medium.ttf");
            setTypeface(tf);
        }
    }
}
