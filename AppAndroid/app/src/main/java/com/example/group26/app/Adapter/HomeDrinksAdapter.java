package com.example.group26.app.Adapter;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.group26.app.Model.Drinks;
import com.example.group26.app.R;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class HomeDrinksAdapter extends BaseAdapter {
    Context context;
    ArrayList<Drinks> drinksArrayList;

    public HomeDrinksAdapter(Context context, ArrayList<Drinks> drinksArrayList) {
        this.context = context;
        this.drinksArrayList = drinksArrayList;
    }

    @Override
    public int getCount() {
        return drinksArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return drinksArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView=inflater.inflate(R.layout.drinks,null);


        ImageView ivDrinks=(ImageView) convertView.findViewById(R.id.ivDrinks);
        TextView tvDrinksName=(TextView) convertView.findViewById(R.id.tvDrinksName);
        TextView tvPrice=(TextView) convertView.findViewById(R.id.tvPrice);

        Drinks drinks=drinksArrayList.get(position);

        tvDrinksName.setText(drinks.getDrinksName());
        DecimalFormat decimalFormat=new DecimalFormat("###,###,###");
        tvPrice.setText("Giá : "+decimalFormat.format(drinks.getPrice())+"Đ");
        ivDrinks.setImageBitmap(BitmapFactory.decodeByteArray(drinks.getImageData(),0,drinks.getImageData().length));

        return convertView;
    }
}
