package com.example.group26.app.Fragment;

import android.annotation.SuppressLint;
import android.graphics.drawable.AnimationDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.group26.app.Adapter.DrinksAdapter;
import com.example.group26.app.Model.Drinks;
import com.example.group26.app.Model.URLServlet;
import com.example.group26.app.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class FragmentProduct extends Fragment implements DrinksAdapter.ILoadMore {
    private String title;
    private int page;
    ImageView imgSearch;
    DrinksAdapter drinksAdapter;
    Animation startAnimation;
    ArrayList<Drinks> arrayList;
    RequestQueue requestQueue;
    RecyclerView recyclerViewCate;
    AnimationDrawable animationDrawable;
    ImageView loading;

    // newInstance constructor for creating fragment with arguments
    public static FragmentProduct newInstance(int page, String title) {
        FragmentProduct fragmentFirst = new FragmentProduct();
        Bundle args = new Bundle();
        args.putInt("PAGEINT", page);
        args.putString("CATEGORYID", title);
        fragmentFirst.setArguments(args);
        return fragmentFirst;
    }

    // Store instance variables based on arguments passed
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        page = getArguments().getInt("PAGEINT", 0);
        title = getArguments().getString("CATEGORYID");

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_product, container, false);

        /*TabLayout customTab= (TabLayout) view.findViewById(R.id.tabLayout);

        //AppCompatActivity activity = (AppCompatActivity) getActivity();
        ViewPager viewPager = (ViewPager) view.findViewById(R.id.viewPager);
        customTab.setupWithViewPager(viewPager);
        View root = customTab.getChildAt(0);
        if (root instanceof LinearLayout) {
            ((LinearLayout) root).setShowDividers(LinearLayout.SHOW_DIVIDER_MIDDLE);
            GradientDrawable drawable = new GradientDrawable();
            drawable.setColor(getResources().getColor(R.color.orange));
            drawable.setSize(2, 1);
            ((LinearLayout) root).setDividerPadding(10);
            ((LinearLayout) root).setDividerDrawable(drawable);
        }*/

        loading=view.findViewById(R.id.loading);
        animationDrawable = (AnimationDrawable) loading.getDrawable();

        recyclerViewCate = (RecyclerView) view.findViewById(R.id.recyclerviewCate);
        startAnimation = AnimationUtils.loadAnimation(recyclerViewCate.getContext(), R.anim.bounce);

        arrayList = new ArrayList<>();
        recyclerViewCate.setLayoutManager(new LinearLayoutManager(recyclerViewCate.getContext()));
        drinksAdapter = new DrinksAdapter(recyclerViewCate, recyclerViewCate.getContext(), arrayList);

        drinksAdapter.setLoadMore(this::onLoadMore);

        recyclerViewCate.setHasFixedSize(true);
        recyclerViewCate.setAdapter(drinksAdapter);
        GetData();

        return view;
    }
    private void GetData() {

        loading.setVisibility(View.VISIBLE);
        animationDrawable.start();

        URLServlet urlServlet = new URLServlet();
        requestQueue=Volley.newRequestQueue(recyclerViewCate.getContext());
        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                urlServlet.getURLCategoryServlet(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONArray jsonArray=new JSONArray(response);
                    for (int i=0;i<jsonArray.length();i++) {
                        JSONObject jsonObject= jsonArray.getJSONObject(i);

                        Drinks drinks = jsonToDrinks(jsonObject);
                        if (drinks != null) {
                            arrayList.add(drinks);
                        }
                    }
                    new AsyncTask<String, String, String>() {
                        @Override
                        protected String doInBackground(String... strings) {

                            return null;
                        }

                        @Override
                        protected void onProgressUpdate(String... values) {
                            super.onProgressUpdate(values);
                        }

                        @Override
                        protected void onPostExecute(String s) {
                            super.onPostExecute(s);
                            drinksAdapter.notifyDataSetChanged();
                            drinksAdapter.setLoaded();

                            loading.setVisibility(View.GONE);
                            animationDrawable.stop();
                        }
                    }.execute();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(recyclerViewCate.getContext(), "Lỗi!", Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> data = new HashMap<>();
                data.put("CATEGORYID", title);
                data.put("index", String.valueOf(arrayList.size()));
                return data;
            }
        };
        requestQueue.add(stringRequest);
    }
    @SuppressLint("LongLogTag")
    public static Drinks jsonToDrinks(JSONObject jsonObject) {
        try {
            String drinksID = jsonObject.getString("DrinksID");
            String drinksName = jsonObject.getString("DrinksName");
            Integer price = jsonObject.getInt("Price");
            String description=jsonObject.getString("Description");
            boolean state=jsonObject.getBoolean("State");
            byte[] imageData= Base64.decode(jsonObject.getString("Image"),Base64.DEFAULT);
            String type=jsonObject.getString("Type");
            Integer rate=2;
            Drinks test = new Drinks(drinksID,drinksName,price,description,state,imageData,type,rate);
            Log.d("ABCDEFGH", String.valueOf(test));
            return new Drinks(drinksID,drinksName,price,description,state,imageData,type,rate);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void onLoadMore() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                arrayList.add(null);
                drinksAdapter.notifyItemInserted(arrayList.size() - 1);
                arrayList.remove(arrayList.size() - 1);
                drinksAdapter.notifyItemRemoved(arrayList.size());
                GetData();
            }
        }, 1000);
    }
}

