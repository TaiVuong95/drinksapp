package com.example.group26.app.Model;

import java.io.Serializable;

public class Drinks implements Serializable{
    private String drinksID;
    private String drinksName;
    private Integer price;

    private String description;
    private boolean state;
    private byte[] imageData;
    private String type;

    private Integer rate;

    public Drinks(String drinksID, String drinksName, Integer price, String description, boolean state, byte[] imageData, String type, Integer rate) {
        this.drinksID = drinksID;
        this.drinksName = drinksName;
        this.price = price;
        this.description = description;
        this.state = state;
        this.imageData = imageData;
        this.type = type;
        this.rate = rate;
    }

    public String getDrinksID() {
        return drinksID;
    }

    public void setDrinksID(String drinksID) {
        this.drinksID = drinksID;
    }

    public String getDrinksName() {
        return drinksName;
    }

    public void setDrinksName(String drinksName) {
        this.drinksName = drinksName;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isState() {
        return state;
    }

    public void setState(boolean state) {
        this.state = state;
    }

    public byte[] getImageData() {
        return imageData;
    }

    public void setImageData(byte[] imageData) {
        this.imageData = imageData;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getRate() {
        return rate;
    }

    public void setRate(Integer rate) {
        this.rate = rate;
    }
}
