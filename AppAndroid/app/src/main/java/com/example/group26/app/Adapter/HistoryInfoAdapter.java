package com.example.group26.app.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.group26.app.Model.HistoryShoppingInfo;
import com.example.group26.app.Model.UserInfo;
import com.example.group26.app.R;

import java.util.List;

public class HistoryInfoAdapter extends BaseAdapter {
    Context myContext;
    int myLayout;
    List<HistoryShoppingInfo>arrayHistoryInfo;
    public HistoryInfoAdapter(Context context, int layout, List<HistoryShoppingInfo>list){
        myContext=context;
        myLayout=layout;
        arrayHistoryInfo=list;
    }
    @Override
    public int getCount() {
        return arrayHistoryInfo.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater= (LayoutInflater) myContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView=inflater.inflate(myLayout,null);
        TextView txtName=convertView.findViewById(R.id.txtName);
        txtName.setText(arrayHistoryInfo.get(position).getName());
        TextView txtHistoryInfo=convertView.findViewById(R.id.txtHistoryInfo);
        txtHistoryInfo.setText(arrayHistoryInfo.get(position).getHistoryInfo());
        return convertView;
    }
}
