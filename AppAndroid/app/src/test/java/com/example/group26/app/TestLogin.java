package com.example.group26.app;

import com.example.group26.app.Unittest.Login;
import com.example.group26.app.Unittest.LoginView;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestLogin {
    String user="chinh";
    String pass="chinh1234";
    @Mock
    LoginView view;
    @Mock
    Login login;
    @Test
    public void testUser() throws Exception{
        when(view.getUsername()).thenReturn(user);
        String result="chinh";
        Assert.assertEquals(result,user);
    }
    @Test
    public void testPass() throws Exception{
        String result="chinh1234";
        when(view.getUsername()).thenReturn(pass);
        Assert.assertEquals(result,pass);
    }
    @Test
    public void testLogin() throws Exception{
        when(view.getUsername()).thenReturn("chinh");
        when(view.getPassword()).thenReturn("chinh1234");
        when(login.login("chinh", "chinh1234")).thenReturn(true);
        Assert.assertEquals(true,true);
    }
}
